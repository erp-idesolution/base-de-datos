USE [master]
GO
/****** Object:  Database [db_sistema_erp]    Script Date: 07/04/2021 06:20:12 p. m. ******/
CREATE DATABASE [db_sistema_erp]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_sistema_erp', FILENAME = N'D:\SQLserver_2016_Data\db_sistema_erp.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_sistema_erp_log', FILENAME = N'D:\SQLserver_2016_Data\db_sistema_erp_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [db_sistema_erp] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_sistema_erp].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_sistema_erp] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_sistema_erp] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_sistema_erp] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_sistema_erp] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_sistema_erp] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_sistema_erp] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_sistema_erp] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_sistema_erp] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_sistema_erp] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_sistema_erp] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_sistema_erp] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_sistema_erp] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_sistema_erp] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_sistema_erp] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_sistema_erp] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_sistema_erp] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_sistema_erp] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_sistema_erp] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_sistema_erp] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_sistema_erp] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_sistema_erp] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_sistema_erp] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_sistema_erp] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_sistema_erp] SET  MULTI_USER 
GO
ALTER DATABASE [db_sistema_erp] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_sistema_erp] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_sistema_erp] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_sistema_erp] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_sistema_erp] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_sistema_erp] SET QUERY_STORE = OFF
GO
USE [db_sistema_erp]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [db_sistema_erp]
GO
/****** Object:  User [dvilca]    Script Date: 07/04/2021 06:20:13 p. m. ******/
CREATE USER [dvilca] FOR LOGIN [dvilca] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [dvilca]
GO
ALTER ROLE [db_datareader] ADD MEMBER [dvilca]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [dvilca]
GO
/****** Object:  View [dbo].[v_DatosCliente]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_DatosCliente] AS
    SELECT (ISNULL(nombre, '') + ' ' + ISNULL(apellidoPaterno, '') + ' ' + ISNULL(apellidoMaterno, '')) AS DatosCliente, numDocumento
    FROM TBLPERSONA
    WHERE TBLPERSONA.tipoRegistro = 2


GO
/****** Object:  View [dbo].[v_DatosCliente_ventas]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_DatosCliente_ventas] AS
    SELECT (ISNULL(nombre, '') + ' ' + ISNULL(apellidoPaterno, '') + ' ' + ISNULL(apellidoMaterno,'') + ' ' + ISNULL(numDocumento, '')) AS DatosCliente, numDocumento
    FROM TBLVENTA        
    INNER JOIN TBLPERSONA
    ON TBLVENTA.DocumentoCliente = TBLPERSONA.numDocumento
    WHERE TBLPERSONA.tipoRegistro = 2



GO
/****** Object:  Table [dbo].[TBLCOMPROBANTE]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLCOMPROBANTE](
	[IdComprobante] [int] IDENTITY(1,1) NOT NULL,
	[IdSucursal] [int] NOT NULL,
	[SerieBoleta] [varchar](10) NOT NULL,
	[CorrelativoBoleta] [int] NOT NULL,
	[SerieFactura] [varchar](10) NOT NULL,
	[CorrelativoFactura] [int] NOT NULL,
	[SerieTicket] [varchar](10) NOT NULL,
	[CorrelativoTicket] [int] NOT NULL,
	[Estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdComprobante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLCONCEPTO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLCONCEPTO](
	[prefijo] [int] NOT NULL,
	[correlativo] [int] NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[abreviatura] [varchar](5) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLCONTROLERROR]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLCONTROLERROR](
	[CodigoApi] [int] NOT NULL,
	[DataEntrada] [nvarchar](300) NOT NULL,
	[MetodoError] [nvarchar](50) NOT NULL,
	[MensajeError] [nvarchar](100) NOT NULL,
	[TrazaError] [nvarchar](500) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Sucursal] [int] NOT NULL,
	[DocumentoUsuario] [nvarchar](20) NULL,
	[IdControlError] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdControlError] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLDEPARTAMENTO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLDEPARTAMENTO](
	[IdDepartamento] [int] NOT NULL,
	[NombreDepartamento] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLDISTRITO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLDISTRITO](
	[IdDistrito] [int] NOT NULL,
	[IdProvincia] [int] NOT NULL,
	[IdDepartamento] [int] NOT NULL,
	[NombreDistrito] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLEMPRESA]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLEMPRESA](
	[IdEmpresa] [int] NOT NULL,
	[NombreEmpresa] [nvarchar](50) NOT NULL,
	[LogoEmpresa] [nvarchar](50) NOT NULL,
	[Estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLMODULO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLMODULO](
	[idModulo] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[Url] [nvarchar](30) NULL,
	[Estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idModulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPAGOEFECTIVO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPAGOEFECTIVO](
	[IdPagoEfectivo] [int] IDENTITY(1,1) NOT NULL,
	[SeriePago] [varchar](10) NOT NULL,
	[CorrelativoPago] [int] NOT NULL,
	[NumeroDocumento] [varchar](15) NOT NULL,
	[Importe] [decimal](10, 2) NOT NULL,
	[Efectivo] [decimal](10, 2) NOT NULL,
	[Vuelto] [decimal](10, 2) NOT NULL,
	[IdSucursal] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdVenta] [int] NOT NULL,
	[Estado] [int] NOT NULL,
	[FechaPago] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPagoEfectivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPAGOTRANSFERENCIA]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPAGOTRANSFERENCIA](
	[IdPagoTransferencia] [int] IDENTITY(1,1) NOT NULL,
	[SeriePago] [varchar](10) NOT NULL,
	[CorrelativoPago] [int] NOT NULL,
	[NumeroDocumento] [varchar](15) NOT NULL,
	[Importe] [decimal](10, 2) NOT NULL,
	[CodigoBanco] [int] NOT NULL,
	[NumeroOperacion] [varchar](30) NOT NULL,
	[IdSucursal] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdVenta] [int] NOT NULL,
	[Estado] [int] NOT NULL,
	[FechaPago] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPagoTransferencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPARAMETRO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPARAMETRO](
	[prefijo] [int] NOT NULL,
	[correlativo] [int] NOT NULL,
	[numero1] [int] NULL,
	[numero2] [int] NULL,
	[fecha1] [date] NULL,
	[fecha2] [date] NULL,
	[decimal1] [decimal](8, 2) NULL,
	[decimal2] [decimal](8, 2) NULL,
	[texto1] [varchar](500) NULL,
	[texto2] [varchar](500) NULL,
	[estado] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPERFIL]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPERFIL](
	[idPerfil] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[Estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPERSONA]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPERSONA](
	[idPersona] [int] IDENTITY(1,1) NOT NULL,
	[tipoPersona] [int] NOT NULL,
	[nombre] [varchar](500) NOT NULL,
	[tipoDocumento] [int] NOT NULL,
	[numDocumento] [varchar](15) NOT NULL,
	[email] [varchar](50) NULL,
	[apellidoPaterno] [nvarchar](50) NULL,
	[apellidoMaterno] [nvarchar](50) NULL,
	[estado] [int] NOT NULL,
	[idDepartamento] [int] NULL,
	[idProvincia] [int] NULL,
	[idDistrito] [int] NULL,
	[tipoRegistro] [int] NULL,
	[fechaNacimiento] [date] NULL,
	[numeroContacto] [varchar](10) NULL,
	[direccion] [varchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[idPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPRESENTACION]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPRESENTACION](
	[IdPresentacion] [int] IDENTITY(1,1) NOT NULL,
	[DescripcionPresentacion] [nvarchar](50) NOT NULL,
	[Estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPresentacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPRODUCTO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPRODUCTO](
	[IdProducto] [int] IDENTITY(1,1) NOT NULL,
	[SkuProducto] [nvarchar](50) NOT NULL,
	[DescripcionProducto] [nvarchar](100) NOT NULL,
	[Estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLPROVINCIA]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLPROVINCIA](
	[IdProvincia] [int] NOT NULL,
	[IdDepartamento] [int] NOT NULL,
	[NombreProvincia] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLRELACION_P_M]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLRELACION_P_M](
	[idPerfilModulo] [int] IDENTITY(1,1) NOT NULL,
	[idPerfil] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idModulo] [int] NOT NULL,
	[idSubmodulo] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idPerfilModulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLRELACION_P_P]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLRELACION_P_P](
	[IdProducto] [int] NOT NULL,
	[IdPresentacion] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLRELACION_U_S]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLRELACION_U_S](
	[IdUsuario_rel] [int] NOT NULL,
	[IdSucursal_rel] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLSUBMODULO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLSUBMODULO](
	[idSubmodulo] [int] IDENTITY(1,1) NOT NULL,
	[idModulo] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[Url] [nvarchar](30) NULL,
	[estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idSubmodulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLSUCURSAL]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLSUCURSAL](
	[idSucursal] [int] IDENTITY(1,1) NOT NULL,
	[razonSocial] [varchar](200) NOT NULL,
	[tipoDocumento] [int] NOT NULL,
	[numDocumento] [varchar](15) NOT NULL,
	[direccion] [varchar](200) NOT NULL,
	[telefono] [varchar](13) NOT NULL,
	[email] [varchar](50) NULL,
	[representante] [varchar](200) NULL,
	[IdEmpresa] [int] NULL,
	[estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idSucursal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLUSUARIO]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLUSUARIO](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[idPerfil] [int] NOT NULL,
	[documento] [varchar](15) NOT NULL,
	[cuenta] [varchar](50) NOT NULL,
	[clave] [varchar](200) NOT NULL,
	[sal] [varchar](200) NOT NULL,
	[fechaRegistro] [date] NOT NULL,
	[IdPersona] [int] NULL,
	[estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLVENTA]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLVENTA](
	[IdVenta] [int] IDENTITY(1,1) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[DocumentoCliente] [varchar](20) NOT NULL,
	[FechaActualizacion] [date] NULL,
	[IdSucursal] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[MontoTotal] [decimal](10, 2) NOT NULL,
	[TotalArticulos] [int] NOT NULL,
	[Estado] [int] NOT NULL,
	[Subtotal] [decimal](10, 2) NOT NULL,
	[MontoIGV] [decimal](10, 2) NOT NULL,
	[EstadoPago] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TBLVENTADETALLE]    Script Date: 07/04/2021 06:20:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBLVENTADETALLE](
	[IdDetalleVenta] [int] IDENTITY(1,1) NOT NULL,
	[IdVenta] [int] NOT NULL,
	[SkuProducto] [nvarchar](50) NOT NULL,
	[PrecioProducto] [decimal](10, 2) NOT NULL,
	[CantidadProducto] [int] NOT NULL,
	[ImporteProducto] [decimal](10, 2) NOT NULL,
	[Presentacion] [nvarchar](50) NOT NULL,
	[Item] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDetalleVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TBLCOMPROBANTE] ON 

INSERT [dbo].[TBLCOMPROBANTE] ([IdComprobante], [IdSucursal], [SerieBoleta], [CorrelativoBoleta], [SerieFactura], [CorrelativoFactura], [SerieTicket], [CorrelativoTicket], [Estado]) VALUES (1, 1, N'B020', 1, N'F020', 2, N'T020', 0, 0)
SET IDENTITY_INSERT [dbo].[TBLCOMPROBANTE] OFF
GO
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (1, 0, N'MEDIOS DE PAGO', N'MDPAG')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (1, 1, N'EFECTIVO', N'EFECT')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (1, 2, N'TRANSFERENCIA', N'TRANS')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (2, 0, N'TIPOS DE COMPROBANTE', N'TPCOM')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (2, 1, N'FACTURA', N'FACT')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (2, 2, N'BOLETA', N'bol')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (3, 0, N'BANCOS ASOCIADOS', N'BCO')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (3, 1, N'BANCO DE CREDITO', N'BCP')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (3, 2, N'INTERBANK', N'INT')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (3, 3, N'BANCO DE LA NACION', N'BCON')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (4, 0, N'TIPOS DE PERSONA', N'TPER')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (4, 1, N'NATURAL', N'PNAT')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (4, 2, N'JURIDICA', N'PJUR')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (5, 0, N'TIPOS DE DOCUMENTO', N'TDOC')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (5, 1, N'DNI', N'DNI')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (5, 2, N'RUC', N'RUC')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (6, 0, N'ESTADOS DE VENTA', N'EVEN')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (6, 1, N'REGISTRADO', N'REG')
INSERT [dbo].[TBLCONCEPTO] ([prefijo], [correlativo], [descripcion], [abreviatura]) VALUES (6, 2, N'FACTURADO', N'FAC')
GO
SET IDENTITY_INSERT [dbo].[TBLCONTROLERROR] ON 

INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (1, N'a', N'login', N'error', N'traza', CAST(N'2021-02-25T11:55:42.793' AS DateTime), 20, N'70752618', 1)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (1, N'a', N'login', N'error', N'traza', CAST(N'2021-02-25T12:04:37.743' AS DateTime), 20, N'70752618', 2)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (1, N'a', N'login', N'error', N'traza', CAST(N'2021-02-25T12:11:24.610' AS DateTime), 20, N'70752618', 3)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (1, N'a', N'login', N'error', N'traza', CAST(N'2021-02-25T12:17:27.120' AS DateTime), 20, N'70752618', 4)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (2, N'z', N'tomatodo', N'errorprueba', N'trazaprueba', CAST(N'2021-02-25T15:39:34.843' AS DateTime), 10, N'43240993', 5)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'a', N'login', N'error', N'traza', CAST(N'2021-02-25T16:06:38.740' AS DateTime), 20, N'70752618', 6)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (5, N'a', N'b', N'c', N'd', CAST(N'2021-02-25T18:22:12.700' AS DateTime), 1, N'456', 7)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (5, N'a', N'b', N'c', N'd', CAST(N'2021-02-25T18:22:41.140' AS DateTime), 1, N'456', 8)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'{"FechaCreacion":"string","CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"MontoTotal":"string","Cliente":{"TipoDocumento":0,"NumeroDocumento":"string","Nombres":"string","ApellidoPaterno":"string","ApellidoMaterno":"string"},"DetalleVenta":[{"Precio":"string","Cantidad":"string","Subtotal"', N'AddSale', N'El procedimiento o la función ''pa_registrar_venta'' esperaba el parámetro ''@Mensaje'', que no se ha es', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-02T12:06:22.880' AS DateTime), 0, N'0', 9)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (4, N'"{ Sku: AFK53 }"', N'getDataProduct', N'Object reference not set to an instance of an object.', N'   at WebApi.Models.ProductModel.getDataProduct(String Sku) in /home/dvilca/Documentos/erp/Models/ProductModel.cs:line 41', CAST(N'2021-03-02T19:05:55.687' AS DateTime), 0, N'0', 10)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'""', N'ListSales', N'nombreCliente', N'   at Microsoft.Data.ProviderBase.BasicFieldNameLookup.GetOrdinal(String fieldName)
   at Microsoft.Data.SqlClient.SqlDataReader.GetOrdinal(String name)
   at Microsoft.Data.SqlClient.SqlDataReader.get_Item(String name)
   at WebApi.Models.SaleModel.GetSales() in /home/dvilca/Documentos/erp/Models/SaleModel.cs:line 98', CAST(N'2021-03-03T16:20:22.320' AS DateTime), 0, N'0', 11)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:19:05.673' AS DateTime), 0, N'0', 12)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:20:46.400' AS DateTime), 0, N'0', 13)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:21:24.083' AS DateTime), 0, N'0', 14)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'""', N'ListSales', N'Unable to cast object of type ''System.Int32'' to type ''System.Decimal''.', N'   at WebApi.Models.SaleModel.GetSales() in /home/dvilca/Documentos/erp/Models/SaleModel.cs:line 98', CAST(N'2021-03-03T16:26:38.727' AS DateTime), 0, N'0', 15)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:21:31.707' AS DateTime), 0, N'0', 16)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:21:55.213' AS DateTime), 0, N'0', 17)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:21:57.363' AS DateTime), 0, N'0', 18)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:22:26.643' AS DateTime), 0, N'0', 19)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:27:20.083' AS DateTime), 0, N'0', 20)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (9, N'60', N'GetSaleDetail', N'El procedimiento o la función ''pa_listar_datos_detalle_venta'' esperaba el parámetro ''@id_venta'', que', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-08T18:56:59.637' AS DateTime), 0, N'0', 21)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'El procedimiento o la función ''pa_listar_datos_ventas'' esperaba el parámetro ''@FechaInicio'', que no ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-10T16:31:26.550' AS DateTime), 0, N'0', 22)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'""', N'GetSales', N'Estado', N'   at Microsoft.Data.ProviderBase.BasicFieldNameLookup.GetOrdinal(String fieldName)
   at Microsoft.Data.SqlClient.SqlDataReader.GetOrdinal(String name)
   at Microsoft.Data.SqlClient.SqlDataReader.get_Item(String name)
   at WebApi.Models.SaleModel.GetSales() in /app/Models/SaleModel.cs:line 99', CAST(N'2021-03-10T16:33:00.383' AS DateTime), 0, N'0', 23)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"12/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:53:16.537' AS DateTime), 0, N'0', 24)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"12/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:53:18.680' AS DateTime), 0, N'0', 25)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:53:43.170' AS DateTime), 0, N'0', 26)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"string","FechaFin":"string","DatosCliente":"string","EstadoPago":0,"IdVenta":0}', N'GetSales', N'Error al convertir una cadena de caracteres en fecha y/u hora.', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-11T12:24:38.573' AS DateTime), 0, N'0', 27)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"string","FechaFin":"string","DatosCliente":"string","EstadoPago":0,"IdVenta":1}', N'GetSales', N'Error al convertir una cadena de caracteres en fecha y/u hora.', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-11T12:25:35.690' AS DateTime), 0, N'0', 28)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"12/03/2021","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:21:45.743' AS DateTime), 0, N'0', 29)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"15/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:43:21.093' AS DateTime), 0, N'0', 30)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"15/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:43:32.180' AS DateTime), 0, N'0', 31)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"03/13/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:11:33.573' AS DateTime), 0, N'0', 32)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"03/13/2021","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:12:13.553' AS DateTime), 0, N'0', 33)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"12/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:19:45.513' AS DateTime), 0, N'0', 34)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"15/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:25:11.173' AS DateTime), 0, N'0', 35)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"15/03/2021  ","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:25:33.793' AS DateTime), 0, N'0', 36)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"15/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:44:06.973' AS DateTime), 0, N'0', 37)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"12/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:44:19.250' AS DateTime), 0, N'0', 38)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":null,"FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaInicio'', que no se ha ', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:45:36.563' AS DateTime), 0, N'0', 39)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"15/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:45:48.650' AS DateTime), 0, N'0', 40)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"15/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlComman', CAST(N'2021-03-12T13:47:58.720' AS DateTime), 0, N'0', 41)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:54:47.580' AS DateTime), 0, N'0', 42)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:57:02.837' AS DateTime), 0, N'0', 43)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:57:04.160' AS DateTime), 0, N'0', 44)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:57:04.980' AS DateTime), 0, N'0', 45)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:57:05.183' AS DateTime), 0, N'0', 46)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:57:05.257' AS DateTime), 0, N'0', 47)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:57:55.170' AS DateTime), 0, N'0', 48)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'AddSale', N'Ocurrio un Error: No se puede insertar el valor NULL en la columna ''FechaCreacion'', tabla ''db_sistem', N'pa_registrar_venta', CAST(N'2021-03-16T01:44:08.157' AS DateTime), 0, N'0', 49)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'AddSale', N'Ocurrio un Error: No se puede insertar el valor NULL en la columna ''FechaCreacion'', tabla ''db_sistem', N'pa_registrar_venta', CAST(N'2021-03-16T01:47:03.753' AS DateTime), 0, N'0', 50)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'AddSale', N'Ocurrio un Error: No se puede insertar el valor NULL en la columna ''FechaCreacion'', tabla ''db_sistem', N'pa_registrar_venta', CAST(N'2021-03-16T01:47:31.190' AS DateTime), 0, N'0', 51)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T01:48:08.790' AS DateTime), 0, N'0', 52)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:03:36.303' AS DateTime), 0, N'0', 53)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:08:36.330' AS DateTime), 0, N'0', 54)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:13:24.893' AS DateTime), 0, N'0', 55)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"12/03/2021  12:00:00","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T13:59:03.763' AS DateTime), 0, N'0', 56)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (8, N'{"FechaInicio":"13/03/2021","FechaFin":null,"DatosCliente":"","EstadoPago":0,"IdVenta":0}', N'GetSales', N'El procedimiento o la función ''pa_listar_ventas'' esperaba el parámetro ''@FechaFin'', que no se ha esp', N'   at Microsoft.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at Microsoft.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at Microsoft.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at ', CAST(N'2021-03-12T14:01:39.407' AS DateTime), 0, N'0', 57)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'{"FechaCreacion":"16/03/202115:2:3","CodigoSucursal":1,"CodigoUsuario":1,"NumeroArticulos":1,"Subtotal":33,"MontoTotal":38.94,"MontoIGV":5.94,"Cliente":{"IdUsuario":0,"TipoDocumento":1,"NumeroDocumento":"47073345","Nombres":"wilson","ApellidoPaterno":"vasquez","ApellidoMaterno":"coronado"},"DetalleV', N'AddSale', N'Ocurrio un Error: Error al convertir una cadena de caracteres en fecha y/u hora. en la línea 19.', N'pa_registrar_venta', CAST(N'2021-03-12T15:01:55.610' AS DateTime), 1, N'0', 58)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (3, N'{"FechaCreacion":"16/03/202115:2:42","CodigoSucursal":1,"CodigoUsuario":1,"NumeroArticulos":1,"Subtotal":3,"MontoTotal":3.54,"MontoIGV":0.54,"Cliente":{"IdUsuario":0,"TipoDocumento":1,"NumeroDocumento":"47073345","Nombres":"wilson","ApellidoPaterno":"vasquez","ApellidoMaterno":"coronado"},"DetalleVe', N'AddSale', N'Ocurrio un Error: Error al convertir una cadena de caracteres en fecha y/u hora. en la línea 19.', N'pa_registrar_venta', CAST(N'2021-03-12T15:02:28.570' AS DateTime), 1, N'0', 59)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (10, N'{"TipoComprobante":2,"MedioPago":2,"DocumentoCliente":"70752618","MontoImporte":140.50,"MontoEfectivo":0,"MontoVuelto":0,"CodigoBanco":2,"NumeroOperacion":"HRWLW313","CodigoSucursal":1,"CodigoUsuario":1,"CodigoVenta":74}', N'AddPayment', N'Unable to cast object of type ''System.DBNull'' to type ''System.Int32''.', N'   at WebApi.Models.PaymentModel.AddPayment(AddPaymentRequest addPaymentRequest) in /home/dvilca/Documentos/erp/Models/PaymentModel.cs:line 52', CAST(N'2021-03-13T13:53:00.320' AS DateTime), 1, N'1', 60)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:14:04.413' AS DateTime), 0, N'0', 61)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:20:31.083' AS DateTime), 0, N'0', 62)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T01:55:00.450' AS DateTime), 0, N'0', 63)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T01:57:32.173' AS DateTime), 0, N'0', 64)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T01:57:47.110' AS DateTime), 0, N'0', 65)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T01:59:20.070' AS DateTime), 0, N'0', 66)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:00:11.290' AS DateTime), 0, N'0', 67)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:02:18.987' AS DateTime), 0, N'0', 68)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:02:44.720' AS DateTime), 0, N'0', 69)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:11:06.840' AS DateTime), 0, N'0', 70)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:22:16.943' AS DateTime), 0, N'0', 71)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (11, N'{"CodigoVenta":0,"FechaCreacion":null,"CodigoSucursal":0,"CodigoUsuario":0,"NumeroArticulos":0,"Subtotal":0,"MontoTotal":0,"MontoIGV":0,"Cliente":null,"DetalleVenta":null}', N'EditSaleDetail', N'Actualización OK', N'pa_editar_detalle_venta', CAST(N'2021-03-16T02:23:45.323' AS DateTime), 0, N'0', 72)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (13, N'{"NumeroDocumento":"","DatosCliente":"","TipoPersona":0}', N'GetCustomers', N'String '''' was not recognized as a valid DateTime.', N'   at System.DateTimeParse.Parse(ReadOnlySpan`1 s, DateTimeFormatInfo dtfi, DateTimeStyles styles)
   at System.Convert.ToDateTime(String value)
   at WebApi.Models.CustomerModel.GetCustomers(ListCustomersRequest listCustomersRequest) in /home/dvilca/Documentos/erp/Models/CustomerModel.cs:line 58', CAST(N'2021-03-19T16:06:40.400' AS DateTime), 0, N'0', 73)
INSERT [dbo].[TBLCONTROLERROR] ([CodigoApi], [DataEntrada], [MetodoError], [MensajeError], [TrazaError], [Fecha], [Sucursal], [DocumentoUsuario], [IdControlError]) VALUES (13, N'{"NumeroDocumento":"","DatosCliente":"","TipoPersona":0}', N'GetCustomers', N'String '''' was not recognized as a valid DateTime.', N'   at System.DateTimeParse.Parse(ReadOnlySpan`1 s, DateTimeFormatInfo dtfi, DateTimeStyles styles)
   at System.Convert.ToDateTime(String value)
   at WebApi.Models.CustomerModel.GetCustomers(ListCustomersRequest listCustomersRequest) in /home/dvilca/Documentos/erp/Models/CustomerModel.cs:line 48', CAST(N'2021-03-19T15:52:51.407' AS DateTime), 0, N'0', 74)
SET IDENTITY_INSERT [dbo].[TBLCONTROLERROR] OFF
GO
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (1, N'AMAZONAS')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (2, N'ANCASH')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (3, N'APURIMAC')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (4, N'AREQUIPA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (5, N'AYACUCHO')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (6, N'CAJAMARCA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (7, N'CALLAO')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (8, N'CUSCO')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (9, N'HUANCAVELICA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (10, N'HUANUCO')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (11, N'ICA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (12, N'JUNIN')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (13, N'LA LIBERTAD')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (14, N'LAMBAYEQUE')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (15, N'LIMA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (16, N'LORETO')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (17, N'MADRE DE DIOS')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (18, N'MOQUEGUA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (19, N'PASCO')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (20, N'PIURA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (21, N'PUNO')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (22, N'SAN MARTIN')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (23, N'TACNA')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (24, N'TUMBES')
INSERT [dbo].[TBLDEPARTAMENTO] ([IdDepartamento], [NombreDepartamento]) VALUES (25, N'UCAYALI')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 1, N'CHACHAPOYAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 1, N'ASUNCION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 1, N'BALSAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 1, N'CHETO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 1, N'CHILIQUIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 1, N'CHUQUIBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 1, N'GRANADA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 1, N'HUANCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 1, N'LA JALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 1, N'LEIMEBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 1, N'LEVANTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 1, N'MAGDALENA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 1, N'MARISCAL CASTILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 1, N'MOLINOPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 1, 1, N'MONTEVIDEO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 1, 1, N'OLLEROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 1, 1, N'QUINJALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 1, 1, N'SAN FRANCISCO DE DAGUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 1, 1, N'SAN ISIDRO DE MAINO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 1, 1, N'SOLOCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 1, 1, N'SONCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 1, N'BAGUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 1, N'ARAMANGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 1, N'COPALLIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 1, N'EL PARCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 1, N'IMAZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 1, N'LA PECA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 1, N'JUMBILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 1, N'CHISQUILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 1, N'CHURUJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 1, N'COROSHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 1, N'CUISPES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 1, N'FLORIDA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 1, N'JAZAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 1, N'RECTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 3, 1, N'SAN CARLOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 3, 1, N'SHIPASBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 3, 1, N'VALERA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 3, 1, N'YAMBRASBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 1, N'NIEVA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 1, N'EL CENEPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 1, N'RIO SANTIAGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 1, N'LAMUD')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 1, N'CAMPORREDONDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 1, N'COCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 1, N'COLCAMAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 1, N'CONILA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 1, N'INGUILPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 1, N'LONGUITA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 1, N'LONYA CHICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 1, N'LUYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 1, N'LUYA VIEJO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 1, N'MARIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 5, 1, N'OCALLI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 5, 1, N'OCUMAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 5, 1, N'PISUQUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 5, 1, N'PROVIDENCIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 5, 1, N'SAN CRISTOBAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 5, 1, N'SAN FRANCISCO DEL YESO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 5, 1, N'SAN JERONIMO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 5, 1, N'SAN JUAN DE LOPECANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 5, 1, N'SANTA CATALINA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 5, 1, N'SANTO TOMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 5, 1, N'TINGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (23, 5, 1, N'TRITA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 1, N'SAN NICOLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 1, N'CHIRIMOTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 1, N'COCHAMAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 1, N'HUAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 1, N'LIMABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 1, N'LONGAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 1, N'MARISCAL BENAVIDES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 1, N'MILPUC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 6, 1, N'OMIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 1, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 6, 1, N'TOTORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 6, 1, N'VISTA ALEGRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 1, N'BAGUA GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 1, N'CAJARURO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 1, N'CUMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 1, N'EL MILAGRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 1, N'JAMALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 1, N'LONYA GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 1, N'YAMON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 2, N'HUARAZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 2, N'COCHABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 2, N'COLCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 2, N'HUANCHAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 2, N'INDEPENDENCIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 2, N'JANGAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 2, N'LA LIBERTAD')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 2, N'OLLEROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 2, N'PAMPAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 2, N'PARIACOTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 2, N'PIRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 2, N'TARICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 2, N'AIJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 2, N'CORIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 2, N'HUACLLAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 2, N'LA MERCED')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 2, N'SUCCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 2, N'LLAMELLIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 2, N'ACZO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 2, N'CHACCHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 2, N'CHINGAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 2, N'MIRGAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 2, N'SAN JUAN DE RONTOY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 2, N'CHACAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 2, N'ACOCHACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 2, N'CHIQUIAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 2, N'ABELARDO PARDO LEZAMETA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 2, N'ANTONIO RAYMONDI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 2, N'AQUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 2, N'CAJACAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 2, N'CANIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 2, N'COLQUIOC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 2, N'HUALLANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 2, N'HUASTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 2, N'HUAYLLACAYAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 2, N'LA PRIMAVERA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 5, 2, N'MANGAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 5, 2, N'PACLLON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 5, 2, N'SAN MIGUEL DE CORPANQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 5, 2, N'TICLLOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 2, N'CARHUAZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 2, N'ACOPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 2, N'AMASHCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 2, N'ANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 2, N'ATAQUERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 2, N'MARCARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 2, N'PARIAHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 2, N'SAN MIGUEL DE ACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 6, 2, N'SHILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 2, N'TINCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 6, 2, N'YUNGAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 2, N'SAN LUIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 2, N'SAN NICOLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 2, N'YAUYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 2, N'CASMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 2, N'BUENA VISTA ALTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 2, N'COMANDANTE NOEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 2, N'YAUTAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 2, N'CORONGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 2, N'ACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 2, N'BAMBAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 2, N'CUSCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 2, N'LA PAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 2, N'YANAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 9, 2, N'YUPAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 2, N'HUARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 2, N'ANRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 2, N'CAJAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 2, N'CHAVIN DE HUANTAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 2, N'HUACACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 10, 2, N'HUACCHIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 10, 2, N'HUACHIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 10, 2, N'HUANTAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 10, 2, N'MASIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 10, 2, N'PAUCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 10, 2, N'PONTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 10, 2, N'RAHUAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 10, 2, N'RAPAYAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 10, 2, N'SAN MARCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 10, 2, N'SAN PEDRO DE CHANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 10, 2, N'UCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 11, 2, N'HUARMEY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 11, 2, N'COCHAPETI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 11, 2, N'CULEBRAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 11, 2, N'HUAYAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 11, 2, N'MALVAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 12, 2, N'CARAZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 12, 2, N'HUALLANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 12, 2, N'HUATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 12, 2, N'HUAYLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 12, 2, N'MATO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 12, 2, N'PAMPAROMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 12, 2, N'PUEBLO LIBRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 12, 2, N'SANTA CRUZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 12, 2, N'SANTO TORIBIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 12, 2, N'YURACMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 13, 2, N'PISCOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 13, 2, N'CASCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 13, 2, N'ELEAZAR GUZMAN BARRON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 13, 2, N'FIDEL OLIVAS ESCUDERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 13, 2, N'LLAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 13, 2, N'LLUMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 13, 2, N'LUCMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 13, 2, N'MUSGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 14, 2, N'OCROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 14, 2, N'ACAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 14, 2, N'CAJAMARQUILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 14, 2, N'CARHUAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 14, 2, N'COCHAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 14, 2, N'CONGAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 14, 2, N'LLIPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 14, 2, N'SAN CRISTOBAL DE RAJAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 14, 2, N'SAN PEDRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 14, 2, N'SANTIAGO DE CHILCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 15, 2, N'CABANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 15, 2, N'BOLOGNESI')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 15, 2, N'CONCHUCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 15, 2, N'HUACASCHUQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 15, 2, N'HUANDOVAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 15, 2, N'LACABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 15, 2, N'LLAPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 15, 2, N'PALLASCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 15, 2, N'PAMPAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 15, 2, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 15, 2, N'TAUCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 16, 2, N'POMABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 16, 2, N'HUAYLLAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 16, 2, N'PAROBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 16, 2, N'QUINUABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 17, 2, N'RECUAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 17, 2, N'CATAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 17, 2, N'COTAPARACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 17, 2, N'HUAYLLAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 17, 2, N'LLACLLIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 17, 2, N'MARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 17, 2, N'PAMPAS CHICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 17, 2, N'PARARIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 17, 2, N'TAPACOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 17, 2, N'TICAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 18, 2, N'CHIMBOTE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 18, 2, N'CACERES DEL PERU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 18, 2, N'COISHCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 18, 2, N'MACATE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 18, 2, N'MORO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 18, 2, N'NEPEA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 18, 2, N'SAMANCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 18, 2, N'SANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 18, 2, N'NUEVO CHIMBOTE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 19, 2, N'SIHUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 19, 2, N'ACOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 19, 2, N'ALFONSO UGARTE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 19, 2, N'CASHAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 19, 2, N'CHINGALPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 19, 2, N'HUAYLLABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 19, 2, N'QUICHES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 19, 2, N'RAGASH')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 19, 2, N'SAN JUAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 19, 2, N'SICSIBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 20, 2, N'YUNGAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 20, 2, N'CASCAPARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 20, 2, N'MANCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 20, 2, N'MATACOTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 20, 2, N'QUILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 20, 2, N'RANRAHIRCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 20, 2, N'SHUPLUY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 20, 2, N'YANAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 3, N'ABANCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 3, N'CHACOCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 3, N'CIRCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 3, N'CURAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 3, N'HUANIPACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 3, N'LAMBRAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 3, N'PICHIRHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 3, N'SAN PEDRO DE CACHORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 3, N'TAMBURCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 3, N'HUAYANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 3, N'KISHUARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 3, N'PACOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 2, 3, N'PACUCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 2, 3, N'PAMPACHIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 2, 3, N'POMACOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 2, 3, N'SAN ANTONIO DE CACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 2, 3, N'SAN JERONIMO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 2, 3, N'SAN MIGUEL DE CHACCRAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 2, 3, N'SANTA MARIA DE CHICMO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 2, 3, N'TALAVERA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 2, 3, N'TUMAY HUARACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 2, 3, N'TURPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 2, 3, N'KAQUIABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 2, 3, N'JOSE MARIA ARGUEDAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 3, N'ANTABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 3, N'EL ORO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 3, N'HUAQUIRCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 3, N'JUAN ESPINOZA MEDRANO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 3, N'OROPESA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 3, N'PACHACONAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 3, N'SABAINO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 3, N'CHALHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 3, N'CAPAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 3, N'CARAYBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 3, N'CHAPIMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 3, N'COLCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 3, N'COTARUSE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 3, N'HUAYLLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 3, N'JUSTO APU SAHUARAURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 4, 3, N'LUCRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 4, 3, N'POCOHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 4, 3, N'SAN JUAN DE CHACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 4, 3, N'SAAYCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 4, 3, N'SORAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 4, 3, N'TAPAIRIHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 4, 3, N'TINTAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 4, 3, N'TORAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 4, 3, N'YANACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 3, N'TAMBOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 3, N'COTABAMBAS')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 3, N'COYLLURQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 3, N'HAQUIRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 3, N'MARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 3, N'CHALLHUAHUACHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 3, N'CHINCHEROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 3, N'ANCO_HUALLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 3, N'COCHARCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 3, N'HUACCANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 3, N'OCOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 3, N'ONGOY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 3, N'URANMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 3, N'RANRACANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 3, N'CHUQUIBAMBILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 3, N'CURPAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 3, N'GAMARRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 3, N'HUAYLLATI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 3, N'MAMARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 3, N'MICAELA BASTIDAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 3, N'PATAYPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 7, 3, N'PROGRESO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 7, 3, N'SAN ANTONIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 7, 3, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 7, 3, N'TURPAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 7, 3, N'VILCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 7, 3, N'VIRUNDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 7, 3, N'CURASCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 4, N'AREQUIPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 4, N'ALTO SELVA ALEGRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 4, N'CAYMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 4, N'CERRO COLORADO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 4, N'CHARACATO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 4, N'CHIGUATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 4, N'JACOBO HUNTER')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 4, N'LA JOYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 4, N'MARIANO MELGAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 4, N'MIRAFLORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 4, N'MOLLEBAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 4, N'PAUCARPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 4, N'POCSI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 4, N'POLOBAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 1, 4, N'QUEQUEA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 1, 4, N'SABANDIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 1, 4, N'SACHACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 1, 4, N'SAN JUAN DE SIGUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 1, 4, N'SAN JUAN DE TARUCANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 1, 4, N'SANTA ISABEL DE SIGUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 1, 4, N'SANTA RITA DE SIGUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 1, 4, N'SOCABAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (23, 1, 4, N'TIABAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (24, 1, 4, N'UCHUMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (25, 1, 4, N'VITOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (26, 1, 4, N'YANAHUARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (27, 1, 4, N'YARABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (28, 1, 4, N'YURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (29, 1, 4, N'JOSE LUIS BUSTAMANTE Y RIVERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 4, N'CAMANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 4, N'JOSE MARIA QUIMPER')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 4, N'MARIANO NICOLAS VALCARCEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 4, N'MARISCAL CACERES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 4, N'NICOLAS DE PIEROLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 4, N'OCOA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 4, N'QUILCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 4, N'SAMUEL PASTOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 4, N'CARAVELI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 4, N'ACARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 4, N'ATICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 4, N'ATIQUIPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 4, N'BELLA UNION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 4, N'CAHUACHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 4, N'CHALA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 4, N'CHAPARRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 3, 4, N'HUANUHUANU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 3, 4, N'JAQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 3, 4, N'LOMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 3, 4, N'QUICACHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 3, 4, N'YAUCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 4, N'APLAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 4, N'ANDAGUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 4, N'AYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 4, N'CHACHAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 4, N'CHILCAYMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 4, N'CHOCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 4, N'HUANCARQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 4, N'MACHAGUAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 4, 4, N'ORCOPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 4, 4, N'PAMPACOLCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 4, 4, N'TIPAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 4, 4, N'UON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 4, 4, N'URACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 4, 4, N'VIRACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 4, N'CHIVAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 4, N'ACHOMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 4, N'CABANACONDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 4, N'CALLALLI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 4, N'CAYLLOMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 4, N'COPORAQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 4, N'HUAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 4, N'HUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 4, N'ICHUPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 4, N'LARI')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 4, N'LLUTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 5, 4, N'MACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 5, 4, N'MADRIGAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 5, 4, N'SAN ANTONIO DE CHUCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 5, 4, N'SIBAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 5, 4, N'TAPAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 5, 4, N'TISCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 5, 4, N'TUTI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 5, 4, N'YANQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 5, 4, N'MAJES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 4, N'CHUQUIBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 4, N'ANDARAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 4, N'CAYARANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 4, N'CHICHAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 4, N'IRAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 4, N'RIO GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 4, N'SALAMANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 4, N'YANAQUIHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 4, N'MOLLENDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 4, N'COCACHACRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 4, N'DEAN VALDIVIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 4, N'ISLAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 4, N'MEJIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 4, N'PUNTA DE BOMBON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 4, N'COTAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 4, N'ALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 4, N'CHARCANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 4, N'HUAYNACOTAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 4, N'PAMPAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 4, N'PUYCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 4, N'QUECHUALLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 4, N'SAYLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 4, N'TAURIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 8, 4, N'TOMEPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 8, 4, N'TORO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 5, N'AYACUCHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 5, N'ACOCRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 5, N'ACOS VINCHOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 5, N'CARMEN ALTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 5, N'CHIARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 5, N'OCROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 5, N'PACAYCASA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 5, N'QUINUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 5, N'SAN JOSE DE TICLLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 5, N'SAN JUAN BAUTISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 5, N'SANTIAGO DE PISCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 5, N'SOCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 5, N'TAMBILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 5, N'VINCHOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 1, 5, N'JESUS NAZARENO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 5, N'CANGALLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 5, N'CHUSCHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 5, N'LOS MOROCHUCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 5, N'MARIA PARADO DE BELLIDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 5, N'PARAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 5, N'TOTOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 5, N'SANCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 5, N'CARAPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 5, N'SACSAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 5, N'SANTIAGO DE LUCANAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 5, N'HUANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 5, N'AYAHUANCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 5, N'HUAMANGUILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 5, N'IGUAIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 5, N'LURICOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 5, N'SANTILLANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 5, N'SIVIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 5, N'LLOCHEGUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 5, N'SAN MIGUEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 5, N'ANCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 5, N'AYNA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 5, N'CHILCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 5, N'CHUNGUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 5, N'LUIS CARRANZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 5, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 5, N'TAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 5, N'SAMUGARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 5, N'PUQUIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 5, N'AUCARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 5, N'CABANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 5, N'CARMEN SALCEDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 5, N'CHAVIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 5, N'CHIPAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 5, N'HUAC-HUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 5, N'LARAMATE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 6, 5, N'LEONCIO PRADO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 5, N'LLAUTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 6, 5, N'LUCANAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 6, 5, N'OCAA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 6, 5, N'OTOCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 6, 5, N'SAISA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 6, 5, N'SAN CRISTOBAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 6, 5, N'SAN JUAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 6, 5, N'SAN PEDRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 6, 5, N'SAN PEDRO DE PALCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 6, 5, N'SANCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 6, 5, N'SANTA ANA DE HUAYCAHUACHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 6, 5, N'SANTA LUCIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 5, N'CORACORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 5, N'CHUMPI')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 5, N'CORONEL CASTAEDA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 5, N'PACAPAUSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 5, N'PULLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 5, N'PUYUSCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 5, N'SAN FRANCISCO DE RAVACAYCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 7, 5, N'UPAHUACHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 5, N'PAUSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 5, N'COLTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 5, N'CORCULLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 5, N'LAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 5, N'MARCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 5, N'OYOLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 5, N'PARARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 5, N'SAN JAVIER DE ALPABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 5, N'SAN JOSE DE USHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 8, 5, N'SARA SARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 5, N'QUEROBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 5, N'BELEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 5, N'CHALCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 5, N'CHILCAYOC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 5, N'HUACAA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 5, N'MORCOLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 9, 5, N'PAICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 9, 5, N'SAN PEDRO DE LARCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 9, 5, N'SAN SALVADOR DE QUIJE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 9, 5, N'SANTIAGO DE PAUCARAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 9, 5, N'SORAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 5, N'HUANCAPI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 5, N'ALCAMENCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 5, N'APONGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 5, N'ASQUIPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 5, N'CANARIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 10, 5, N'CAYARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 10, 5, N'COLCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 10, 5, N'HUAMANQUIQUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 10, 5, N'HUANCARAYLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 10, 5, N'HUAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 10, 5, N'SARHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 10, 5, N'VILCANCHOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 11, 5, N'VILCAS HUAMAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 11, 5, N'ACCOMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 11, 5, N'CARHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 11, 5, N'CONCEPCION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 11, 5, N'HUAMBALPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 11, 5, N'INDEPENDENCIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 11, 5, N'SAURAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 11, 5, N'VISCHONGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 6, N'CAJAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 6, N'ASUNCION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 6, N'CHETILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 6, N'COSPAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 6, N'ENCAADA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 6, N'JESUS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 6, N'LLACANORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 6, N'LOS BAOS DEL INCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 6, N'MAGDALENA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 6, N'MATARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 6, N'NAMORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 6, N'SAN JUAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 6, N'CAJABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 6, N'CACHACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 6, N'CONDEBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 6, N'SITACOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 6, N'CELENDIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 6, N'CHUMUCH')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 6, N'CORTEGANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 6, N'HUASMIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 6, N'JORGE CHAVEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 6, N'JOSE GALVEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 6, N'MIGUEL IGLESIAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 6, N'OXAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 3, 6, N'SOROCHUCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 3, 6, N'SUCRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 3, 6, N'UTCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 3, 6, N'LA LIBERTAD DE PALLAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 6, N'CHOTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 6, N'ANGUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 6, N'CHADIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 6, N'CHIGUIRIP')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 6, N'CHIMBAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 6, N'CHOROPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 6, N'COCHABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 6, N'CONCHAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 4, 6, N'HUAMBOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 4, 6, N'LAJAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 4, 6, N'LLAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 4, 6, N'MIRACOSTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 4, 6, N'PACCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 4, 6, N'PION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 4, 6, N'QUEROCOTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 4, 6, N'SAN JUAN DE LICUPIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 4, 6, N'TACABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 4, 6, N'TOCMOCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 4, 6, N'CHALAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 6, N'CONTUMAZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 6, N'CHILETE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 6, N'CUPISNIQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 6, N'GUZMANGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 6, N'SAN BENITO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 6, N'SANTA CRUZ DE TOLED')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 6, N'TANTARICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 6, N'YONAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 6, N'CUTERVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 6, N'CALLAYUC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 6, N'CHOROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 6, N'CUJILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 6, N'LA RAMADA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 6, N'PIMPINGOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 6, N'QUEROCOTILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 6, N'SAN ANDRES DE CUTERVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 6, 6, N'SAN JUAN DE CUTERVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 6, N'SAN LUIS DE LUCMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 6, 6, N'SANTA CRUZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 6, 6, N'SANTO DOMINGO DE LA CAPILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 6, 6, N'SANTO TOMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 6, 6, N'SOCOTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 6, 6, N'TORIBIO CASANOVA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 6, N'BAMBAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 6, N'CHUGUR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 6, N'HUALGAYOC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 6, N'JAEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 6, N'BELLAVISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 6, N'CHONTALI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 6, N'COLASAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 6, N'HUABAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 6, N'LAS PIRIAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 6, N'POMAHUACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 6, N'PUCARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 6, N'SALLIQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 8, 6, N'SAN FELIPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 8, 6, N'SAN JOSE DEL ALTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 8, 6, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 6, N'SAN IGNACIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 6, N'CHIRINOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 6, N'HUARANGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 6, N'LA COIPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 6, N'NAMBALLE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 6, N'SAN JOSE DE LOURDES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 9, 6, N'TABACONAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 6, N'PEDRO GALVEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 6, N'CHANCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 6, N'EDUARDO VILLANUEVA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 6, N'GREGORIO PITA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 6, N'ICHOCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 10, 6, N'JOSE MANUEL QUIROZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 10, 6, N'JOSE SABOGAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 11, 6, N'SAN MIGUEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 11, 6, N'BOLIVAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 11, 6, N'CALQUIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 11, 6, N'CATILLUC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 11, 6, N'EL PRADO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 11, 6, N'LA FLORIDA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 11, 6, N'LLAPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 11, 6, N'NANCHOC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 11, 6, N'NIEPOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 11, 6, N'SAN GREGORIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 11, 6, N'SAN SILVESTRE DE COCHAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 11, 6, N'TONGOD')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 11, 6, N'UNION AGUA BLANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 12, 6, N'SAN PABLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 12, 6, N'SAN BERNARDINO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 12, 6, N'SAN LUIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 12, 6, N'TUMBADEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 13, 6, N'SANTA CRUZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 13, 6, N'ANDABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 13, 6, N'CATACHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 13, 6, N'CHANCAYBAOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 13, 6, N'LA ESPERANZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 13, 6, N'NINABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 13, 6, N'PULAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 13, 6, N'SAUCEPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 13, 6, N'SEXI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 13, 6, N'UTICYACU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 13, 6, N'YAUYUCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 7, N'CALLAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 7, N'BELLAVISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 7, N'CARMEN DE LA LEGUA REYNOSO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 7, N'LA PERLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 7, N'LA PUNTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 7, N'VENTANILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 8, N'CUSCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 8, N'CCORCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 8, N'POROY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 8, N'SAN JERONIMO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 8, N'SAN SEBASTIAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 8, N'SANTIAGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 8, N'SAYLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 8, N'WANCHAQ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 8, N'ACOMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 8, N'ACOPIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 8, N'ACOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 8, N'MOSOC LLACTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 8, N'POMACANCHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 8, N'RONDOCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 8, N'SANGARARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 8, N'ANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 8, N'ANCAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 8, N'CACHIMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 8, N'CHINCHAYPUJIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 8, N'HUAROCONDO')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 8, N'LIMATAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 8, N'MOLLEPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 8, N'PUCYURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 3, 8, N'ZURITE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 8, N'CALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 8, N'COYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 8, N'LAMAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 8, N'LARES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 8, N'PISAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 8, N'SAN SALVADOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 8, N'TARAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 8, N'YANATILE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 8, N'YANAOCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 8, N'CHECCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 8, N'KUNTURKANKI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 8, N'LANGUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 8, N'LAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 8, N'PAMPAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 8, N'QUEHUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 8, N'TUPAC AMARU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 8, N'SICUANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 8, N'CHECACUPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 8, N'COMBAPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 8, N'MARANGANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 8, N'PITUMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 8, N'SAN PABLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 8, N'SAN PEDRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 8, N'TINTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 8, N'SANTO TOMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 8, N'CAPACMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 8, N'CHAMACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 8, N'COLQUEMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 8, N'LIVITACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 8, N'LLUSCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 8, N'QUIOTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 7, 8, N'VELILLE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 8, N'ESPINAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 8, N'CONDOROMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 8, N'COPORAQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 8, N'OCORURO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 8, N'PALLPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 8, N'PICHIGUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 8, N'SUYCKUTAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 8, N'ALTO PICHIGUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 8, N'SANTA ANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 8, N'ECHARATE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 8, N'HUAYOPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 8, N'MARANURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 8, N'OCOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 8, N'QUELLOUNO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 9, 8, N'KIMBIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 9, 8, N'SANTA TERESA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 9, 8, N'VILCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 9, 8, N'PICHARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 8, N'PARURO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 8, N'ACCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 8, N'CCAPI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 8, N'COLCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 8, N'HUANOQUITE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 10, 8, N'OMACHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 10, 8, N'PACCARITAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 10, 8, N'PILLPINTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 10, 8, N'YAURISQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 11, 8, N'PAUCARTAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 11, 8, N'CAICAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 11, 8, N'CHALLABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 11, 8, N'COLQUEPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 11, 8, N'HUANCARANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 11, 8, N'KOSIPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 12, 8, N'URCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 12, 8, N'ANDAHUAYLILLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 12, 8, N'CAMANTI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 12, 8, N'CCARHUAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 12, 8, N'CCATCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 12, 8, N'CUSIPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 12, 8, N'HUARO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 12, 8, N'LUCRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 12, 8, N'MARCAPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 12, 8, N'OCONGATE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 12, 8, N'OROPESA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 12, 8, N'QUIQUIJANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 13, 8, N'URUBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 13, 8, N'CHINCHERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 13, 8, N'HUAYLLABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 13, 8, N'MACHUPICCHU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 13, 8, N'MARAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 13, 8, N'OLLANTAYTAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 13, 8, N'YUCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 9, N'HUANCAVELICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 9, N'ACOBAMBILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 9, N'ACORIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 9, N'CONAYCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 9, N'CUENCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 9, N'HUACHOCOLPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 9, N'HUAYLLAHUARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 9, N'IZCUCHACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 9, N'LARIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 9, N'MANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 9, N'MARISCAL CACERES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 9, N'MOYA')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 9, N'NUEVO OCCORO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 9, N'PALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 1, 9, N'PILCHACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 1, 9, N'VILCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 1, 9, N'YAULI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 1, 9, N'ASCENSION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 1, 9, N'HUANDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 9, N'ACOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 9, N'ANDABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 9, N'ANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 9, N'CAJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 9, N'MARCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 9, N'PAUCARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 9, N'POMACOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 9, N'ROSARIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 9, N'LIRCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 9, N'ANCHONGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 9, N'CALLANMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 9, N'CCOCHACCASA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 9, N'CHINCHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 9, N'CONGALLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 9, N'HUANCA-HUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 9, N'HUAYLLAY GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 3, 9, N'JULCAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 3, 9, N'SAN ANTONIO DE ANTAPARCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 3, 9, N'SANTO TOMAS DE PATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 3, 9, N'SECCLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 9, N'CASTROVIRREYNA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 9, N'ARMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 9, N'AURAHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 9, N'CAPILLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 9, N'CHUPAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 9, N'COCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 9, N'HUACHOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 9, N'HUAMATAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 4, 9, N'MOLLEPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 4, 9, N'SAN JUAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 4, 9, N'SANTA ANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 4, 9, N'TANTARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 4, 9, N'TICRAPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 9, N'CHURCAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 9, N'ANCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 9, N'CHINCHIHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 9, N'EL CARMEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 9, N'LA MERCED')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 9, N'LOCROJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 9, N'PAUCARBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 9, N'SAN MIGUEL DE MAYOCC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 9, N'SAN PEDRO DE CORIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 9, N'PACHAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 9, N'COSME')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 9, N'HUAYTARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 9, N'AYAVI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 9, N'CORDOVA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 9, N'HUAYACUNDO ARMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 9, N'LARAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 9, N'OCOYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 9, N'PILPICHACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 9, N'QUERCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 6, 9, N'QUITO-ARMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 9, N'SAN ANTONIO DE CUSICANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 6, 9, N'SAN FRANCISCO DE SANGAYAICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 6, 9, N'SAN ISIDRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 6, 9, N'SANTIAGO DE CHOCORVOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 6, 9, N'SANTIAGO DE QUIRAHUARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 6, 9, N'SANTO DOMINGO DE CAPILLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 6, 9, N'TAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 9, N'PAMPAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 9, N'ACOSTAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 9, N'ACRAQUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 9, N'AHUAYCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 9, N'COLCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 9, N'DANIEL HERNANDEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 9, N'HUACHOCOLPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 7, 9, N'HUARIBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 7, 9, N'AHUIMPUQUIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 7, 9, N'PAZOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 7, 9, N'QUISHUAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 7, 9, N'SALCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 7, 9, N'SALCAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 7, 9, N'SAN MARCOS DE ROCCHAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 7, 9, N'SURCUBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 7, 9, N'TINTAY PUNCU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 10, N'HUANUCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 10, N'AMARILIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 10, N'CHINCHAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 10, N'CHURUBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 10, N'MARGOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 10, N'QUISQUI (KICHKI)')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 10, N'SAN FRANCISCO DE CAYRAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 10, N'SAN PEDRO DE CHAULAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 10, N'SANTA MARIA DEL VALLE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 10, N'YARUMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 10, N'PILLCO MARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 10, N'YACUS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 10, N'AMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 10, N'CAYNA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 10, N'COLPAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 10, N'CONCHAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 10, N'HUACAR')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 10, N'SAN FRANCISCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 10, N'SAN RAFAEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 10, N'TOMAY KICHWA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 10, N'LA UNION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 10, N'CHUQUIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 3, 10, N'MARIAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 3, 10, N'PACHAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 3, 10, N'QUIVILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 3, 10, N'RIPAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 3, 10, N'SHUNQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 3, 10, N'SILLAPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (23, 3, 10, N'YANAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 10, N'HUACAYBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 10, N'CANCHABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 10, N'COCHABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 10, N'PINRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 10, N'LLATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 10, N'ARANCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 10, N'CHAVIN DE PARIARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 10, N'JACAS GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 10, N'JIRCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 10, N'MIRAFLORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 10, N'MONZON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 10, N'PUNCHAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 10, N'PUOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 10, N'SINGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 10, N'TANTAMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 10, N'RUPA-RUPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 10, N'DANIEL ALOMIA ROBLES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 10, N'HERMILIO VALDIZAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 10, N'JOSE CRESPO Y CASTILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 10, N'LUYANDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 10, N'MARIANO DAMASO BERAUN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 10, N'HUACRACHUCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 10, N'CHOLON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 10, N'SAN BUENAVENTURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 10, N'PANAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 10, N'CHAGLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 10, N'MOLINO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 10, N'UMARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 10, N'PUERTO INCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 10, N'CODO DEL POZUZO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 10, N'HONORIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 10, N'TOURNAVISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 10, N'YUYAPICHIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 10, N'JESUS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 10, N'BAOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 10, N'JIVIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 10, N'QUEROPALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 10, N'RONDOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 10, 10, N'SAN FRANCISCO DE ASIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 10, 10, N'SAN MIGUEL DE CAURI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 11, 10, N'CHAVINILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 11, 10, N'CAHUAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 11, 10, N'CHACABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 11, 10, N'APARICIO POMARES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 11, 10, N'JACAS CHICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 11, 10, N'OBAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 11, 10, N'PAMPAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 11, 10, N'CHORAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 11, N'ICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 11, N'LA TINGUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 11, N'LOS AQUIJES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 11, N'OCUCAJE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 11, N'PACHACUTEC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 11, N'PARCONA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 11, N'PUEBLO NUEVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 11, N'SALAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 11, N'SAN JOSE DE LOS MOLINOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 11, N'SAN JUAN BAUTISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 11, N'SANTIAGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 11, N'SUBTANJALLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 11, N'TATE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 11, N'YAUCA DEL ROSARIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 11, N'CHINCHA ALTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 11, N'ALTO LARAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 11, N'CHAVIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 11, N'CHINCHA BAJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 11, N'EL CARMEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 11, N'GROCIO PRADO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 11, N'PUEBLO NUEVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 11, N'SAN JUAN DE YANAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 2, 11, N'SAN PEDRO DE HUACARPANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 2, 11, N'SUNAMPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 2, 11, N'TAMBO DE MORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 11, N'NAZCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 11, N'CHANGUILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 11, N'EL INGENIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 11, N'MARCONA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 11, N'VISTA ALEGRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 11, N'PALPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 11, N'LLIPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 11, N'RIO GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 11, N'SANTA CRUZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 11, N'TIBILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 11, N'PISCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 11, N'HUANCANO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 11, N'HUMAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 11, N'INDEPENDENCIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 11, N'PARACAS')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 11, N'SAN ANDRES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 11, N'SAN CLEMENTE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 11, N'TUPAC AMARU INCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 12, N'HUANCAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 12, N'CARHUACALLANGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 12, N'CHACAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 12, N'CHICCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 12, N'CHILCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 12, N'CHONGOS ALTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 12, N'CHUPURO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 12, N'COLCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 12, N'CULLHUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 12, N'EL TAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 1, 12, N'HUACRAPUQUIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 1, 12, N'HUALHUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 1, 12, N'HUANCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 1, 12, N'HUASICANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 1, 12, N'HUAYUCACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 1, 12, N'INGENIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (24, 1, 12, N'PARIAHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (25, 1, 12, N'PILCOMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (26, 1, 12, N'PUCARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (27, 1, 12, N'QUICHUAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (28, 1, 12, N'QUILCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (29, 1, 12, N'SAN AGUSTIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (30, 1, 12, N'SAN JERONIMO DE TUNAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (32, 1, 12, N'SAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (33, 1, 12, N'SAPALLANGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (34, 1, 12, N'SICAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (35, 1, 12, N'SANTO DOMINGO DE ACOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (36, 1, 12, N'VIQUES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 12, N'CONCEPCION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 12, N'ACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 12, N'ANDAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 12, N'CHAMBARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 12, N'COCHAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 12, N'COMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 12, N'HEROINAS TOLEDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 12, N'MANZANARES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 2, 12, N'MARISCAL CASTILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 2, 12, N'MATAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 2, 12, N'MITO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 2, 12, N'NUEVE DE JULIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 2, 12, N'ORCOTUNA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 2, 12, N'SAN JOSE DE QUERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 2, 12, N'SANTA ROSA DE OCOPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 12, N'CHANCHAMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 12, N'PERENE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 12, N'PICHANAQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 12, N'SAN LUIS DE SHUARO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 12, N'SAN RAMON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 12, N'VITOC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 12, N'JAUJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 12, N'ACOLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 12, N'APATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 12, N'ATAURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 12, N'CANCHAYLLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 12, N'CURICACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 12, N'EL MANTARO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 12, N'HUAMALI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 4, 12, N'HUARIPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 4, 12, N'HUERTAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 4, 12, N'JANJAILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 4, 12, N'JULCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 4, 12, N'LEONOR ORDOEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 4, 12, N'LLOCLLAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 4, 12, N'MARCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 4, 12, N'MASMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 4, 12, N'MASMA CHICCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 4, 12, N'MOLINOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 4, 12, N'MONOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 4, 12, N'MUQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 4, 12, N'MUQUIYAUYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 4, 12, N'PACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (23, 4, 12, N'PACCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (24, 4, 12, N'PANCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (25, 4, 12, N'PARCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (26, 4, 12, N'POMACANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (27, 4, 12, N'RICRAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (28, 4, 12, N'SAN LORENZO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (29, 4, 12, N'SAN PEDRO DE CHUNAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (30, 4, 12, N'SAUSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (31, 4, 12, N'SINCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (32, 4, 12, N'TUNAN MARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (33, 4, 12, N'YAULI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (34, 4, 12, N'YAUYOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 12, N'JUNIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 12, N'CARHUAMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 12, N'ONDORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 12, N'ULCUMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 12, N'MAZAMARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 3, N'ANDAHUAYLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 3, N'ANDARAPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 3, N'CHIARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 3, N'HUANCARAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 3, N'HUANCARAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 12, N'TARMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 12, N'ACOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 12, N'HUARICOLCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 12, N'HUASAHUASI')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 12, N'LA UNION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 12, N'PALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 12, N'PALCAMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 7, 12, N'SAN PEDRO DE CAJAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 7, 12, N'TAPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 12, N'LA OROYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 12, N'CHACAPALPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 12, N'HUAY-HUAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 12, N'MARCAPOMACOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 12, N'MOROCOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 12, N'PACCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 12, N'SANTA BARBARA DE CARHUACAYAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 12, N'SANTA ROSA DE SACCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 12, N'SUITUCANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 8, 12, N'YAULI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 12, N'CHUPACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 12, N'AHUAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 12, N'CHONGOS BAJO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 12, N'HUACHAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 12, N'HUAMANCACA CHICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 12, N'SAN JUAN DE ISCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 9, 12, N'SAN JUAN DE JARPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 9, 12, N'TRES DE DICIEMBRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 9, 12, N'YANACANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 13, N'TRUJILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 13, N'EL PORVENIR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 13, N'FLORENCIA DE MORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 13, N'HUANCHACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 13, N'LA ESPERANZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 13, N'LAREDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 13, N'MOCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 13, N'POROTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 13, N'SALAVERRY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 13, N'SIMBAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 13, N'VICTOR LARCO HERRERA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 13, N'ASCOPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 13, N'CHICAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 13, N'CHOCOPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 13, N'MAGDALENA DE CAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 13, N'PAIJAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 13, N'RAZURI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 13, N'SANTIAGO DE CAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 13, N'CASA GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 13, N'BOLIVAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 13, N'BAMBAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 13, N'CONDORMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 13, N'LONGOTEA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 13, N'UCHUMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 13, N'UCUNCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 13, N'CHEPEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 13, N'PACANGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 13, N'PUEBLO NUEVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 13, N'JULCAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 13, N'CALAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 13, N'CARABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 13, N'HUASO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 13, N'OTUZCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 13, N'AGALLPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 13, N'CHARAT')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 13, N'HUARANCHAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 13, N'LA CUESTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 13, N'MACHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 13, N'PARANDAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 6, 13, N'SALPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 6, 13, N'SINSICAP')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 6, 13, N'USQUIL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 13, N'SAN PEDRO DE LLOC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 13, N'GUADALUPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 13, N'JEQUETEPEQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 13, N'PACASMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 13, N'SAN JOSE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 13, N'TAYABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 13, N'BULDIBUYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 13, N'CHILLIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 13, N'HUANCASPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 13, N'HUAYLILLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 13, N'HUAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 13, N'ONGON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 13, N'PARCOY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 13, N'PATAZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 8, 13, N'PIAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 8, 13, N'SANTIAGO DE CHALLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 8, 13, N'TAURIJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 8, 13, N'URPAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 13, N'HUAMACHUCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 13, N'CHUGAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 13, N'COCHORCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 13, N'CURGOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 13, N'MARCABAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 13, N'SANAGORAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 9, 13, N'SARIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 9, 13, N'SARTIMBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 13, N'SANTIAGO DE CHUCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 13, N'ANGASMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 13, N'CACHICADAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 13, N'MOLLEBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 13, N'MOLLEPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 10, 13, N'QUIRUVILCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 10, 13, N'SANTA CRUZ DE CHUCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 10, 13, N'SITABAMBA')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 11, 13, N'CASCAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 11, 13, N'LUCMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 11, 13, N'MARMOT')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 11, 13, N'SAYAPULLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 12, 13, N'VIRU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 12, 13, N'CHAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 12, 13, N'GUADALUPITO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 14, N'CHICLAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 14, N'CHONGOYAPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 14, N'ETEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 14, N'ETEN PUERTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 14, N'JOSE LEONARDO ORTIZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 14, N'LA VICTORIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 14, N'LAGUNAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 14, N'MONSEFU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 14, N'NUEVA ARICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 14, N'OYOTUN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 14, N'PICSI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 14, N'PIMENTEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 14, N'REQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 14, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 1, 14, N'SAA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 1, 14, N'CAYALTI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 1, 14, N'PATAPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 1, 14, N'POMALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 1, 14, N'PUCALA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 1, 14, N'TUMAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 14, N'FERREAFE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 14, N'CAARIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 14, N'INCAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 14, N'MANUEL ANTONIO MESONES MURO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 14, N'PITIPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 14, N'PUEBLO NUEVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 14, N'LAMBAYEQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 14, N'CHOCHOPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 14, N'ILLIMO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 14, N'JAYANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 14, N'MOCHUMI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 14, N'MORROPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 14, N'MOTUPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 14, N'OLMOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 3, 14, N'PACORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 3, 14, N'SALAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 3, 14, N'SAN JOSE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 3, 14, N'TUCUME')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 15, N'LIMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 15, N'ANCON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 15, N'ATE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 15, N'BARRANCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 15, N'BREA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 15, N'CARABAYLLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 15, N'CHACLACAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 15, N'CHORRILLOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 15, N'CIENEGUILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 15, N'COMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 15, N'EL AGUSTINO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 15, N'INDEPENDENCIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 15, N'JESUS MARIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 15, N'LA MOLINA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 1, 15, N'LA VICTORIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 1, 15, N'LINCE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 1, 15, N'LOS OLIVOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 1, 15, N'LURIGANCHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 1, 15, N'LURIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 1, 15, N'MAGDALENA DEL MAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 1, 15, N'PUEBLO LIBRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 1, 15, N'MIRAFLORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (23, 1, 15, N'PACHACAMAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (24, 1, 15, N'PUCUSANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (25, 1, 15, N'PUENTE PIEDRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (26, 1, 15, N'PUNTA HERMOSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (27, 1, 15, N'PUNTA NEGRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (28, 1, 15, N'RIMAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (29, 1, 15, N'SAN BARTOLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (30, 1, 15, N'SAN BORJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (31, 1, 15, N'SAN ISIDRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (32, 1, 15, N'SAN JUAN DE LURIGANCHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (33, 1, 15, N'SAN JUAN DE MIRAFLORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (34, 1, 15, N'SAN LUIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (35, 1, 15, N'SAN MARTIN DE PORRES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (36, 1, 15, N'SAN MIGUEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (37, 1, 15, N'SANTA ANITA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (38, 1, 15, N'SANTA MARIA DEL MAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (39, 1, 15, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (40, 1, 15, N'SANTIAGO DE SURCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (41, 1, 15, N'SURQUILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (42, 1, 15, N'VILLA EL SALVADOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (43, 1, 15, N'VILLA MARIA DEL TRIUNFO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 15, N'BARRANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 15, N'PARAMONGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 15, N'PATIVILCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 15, N'SUPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 15, N'SUPE PUERTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 15, N'CAJATAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 15, N'COPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 15, N'GORGOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 15, N'HUANCAPON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 15, N'MANAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 15, N'CANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 15, N'ARAHUAY')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 15, N'HUAMANTANGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 15, N'HUAROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 15, N'LACHAQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 15, N'SAN BUENAVENTURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 15, N'SANTA ROSA DE QUIVES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 15, N'SAN VICENTE DE CAETE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 15, N'ASIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 15, N'CALANGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 15, N'CERRO AZUL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 15, N'CHILCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 15, N'COAYLLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 15, N'IMPERIAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 15, N'LUNAHUANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 15, N'MALA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 15, N'NUEVO IMPERIAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 15, N'PACARAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 5, 15, N'QUILMANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 5, 15, N'SAN ANTONIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 5, 15, N'SAN LUIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 5, 15, N'SANTA CRUZ DE FLORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 5, 15, N'ZUIGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 15, N'HUARAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 15, N'ATAVILLOS ALTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 15, N'ATAVILLOS BAJO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 15, N'AUCALLAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 15, N'CHANCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 15, N'IHUARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 15, N'LAMPIAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 15, N'PACARAOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 6, 15, N'SAN MIGUEL DE ACOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 15, N'SANTA CRUZ DE ANDAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 6, 15, N'SUMBILCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 6, 15, N'VEINTISIETE DE NOVIEMBRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 15, N'MATUCANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 15, N'ANTIOQUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 15, N'CALLAHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 15, N'CARAMPOMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 15, N'CHICLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 15, N'CUENCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 15, N'HUACHUPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 7, 15, N'HUANZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 7, 15, N'HUAROCHIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 7, 15, N'LAHUAYTAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 7, 15, N'LANGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 7, 15, N'LARAOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 7, 15, N'MARIATANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 7, 15, N'RICARDO PALMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 7, 15, N'SAN ANDRES DE TUPICOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 7, 15, N'SAN ANTONIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 7, 15, N'SAN BARTOLOME')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 7, 15, N'SAN DAMIAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 7, 15, N'SAN JUAN DE IRIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 7, 15, N'SAN JUAN DE TANTARANCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 7, 15, N'SAN LORENZO DE QUINTI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 7, 15, N'SAN MATEO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (23, 7, 15, N'SAN MATEO DE OTAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (24, 7, 15, N'SAN PEDRO DE CASTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (25, 7, 15, N'SAN PEDRO DE HUANCAYRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (26, 7, 15, N'SANGALLAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (27, 7, 15, N'SANTA CRUZ DE COCACHACRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (28, 7, 15, N'SANTA EULALIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (29, 7, 15, N'SANTIAGO DE ANCHUCAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (30, 7, 15, N'SANTIAGO DE TUNA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (31, 7, 15, N'SANTO DOMINGO DE LOS OLLEROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (32, 7, 15, N'SURCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 15, N'HUACHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 15, N'AMBAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 15, N'CALETA DE CARQUIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 15, N'CHECRAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 15, N'HUALMAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 15, N'HUAURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 15, N'LEONCIO PRADO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 15, N'PACCHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 15, N'SANTA LEONOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 8, 15, N'SANTA MARIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 8, 15, N'SAYAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 8, 15, N'VEGUETA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 15, N'OYON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 15, N'ANDAJES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 15, N'CAUJUL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 15, N'COCHAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 15, N'NAVAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 15, N'PACHANGARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 15, N'YAUYOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 15, N'ALIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 15, N'ALLAUCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 15, N'AYAVIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 15, N'AZANGARO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 10, 15, N'CACRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 10, 15, N'CARANIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 10, 15, N'CATAHUASI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 10, 15, N'CHOCOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 10, 15, N'COCHAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 10, 15, N'COLONIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 10, 15, N'HONGOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 10, 15, N'HUAMPARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 10, 15, N'HUANCAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 10, 15, N'HUANGASCAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 10, 15, N'HUANTAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (17, 10, 15, N'HUAEC')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (18, 10, 15, N'LARAOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (19, 10, 15, N'LINCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (20, 10, 15, N'MADEAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (21, 10, 15, N'MIRAFLORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (22, 10, 15, N'OMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (23, 10, 15, N'PUTINZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (24, 10, 15, N'QUINCHES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (25, 10, 15, N'QUINOCAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (26, 10, 15, N'SAN JOAQUIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (27, 10, 15, N'SAN PEDRO DE PILAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (28, 10, 15, N'TANTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (29, 10, 15, N'TAURIPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (30, 10, 15, N'TOMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (31, 10, 15, N'TUPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (32, 10, 15, N'VIAC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (33, 10, 15, N'VITIS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 16, N'IQUITOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 16, N'ALTO NANAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 16, N'FERNANDO LORES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 16, N'INDIANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 16, N'LAS AMAZONAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 16, N'MAZAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 16, N'NAPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 16, N'PUNCHANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 16, N'PUTUMAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 16, N'TORRES CAUSANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 16, N'BELEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 16, N'SAN JUAN BAUTISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 16, N'TENIENTE MANUEL CLAVERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 16, N'YURIMAGUAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 16, N'BALSAPUERTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 16, N'JEBEROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 16, N'LAGUNAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 2, 16, N'SANTA CRUZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 2, 16, N'TENIENTE CESAR LOPEZ ROJAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 16, N'NAUTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 16, N'PARINARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 16, N'TIGRE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 16, N'TROMPETEROS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 16, N'URARINAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 16, N'RAMON CASTILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 16, N'PEBAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 16, N'YAVARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 16, N'SAN PABLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 16, N'REQUENA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 16, N'ALTO TAPICHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 16, N'CAPELO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 16, N'EMILIO SAN MARTIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 16, N'MAQUIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 16, N'PUINAHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 16, N'SAQUENA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 16, N'SOPLIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 16, N'TAPICHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 16, N'JENARO HERRERA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 16, N'YAQUERANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 16, N'CONTAMANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 16, N'INAHUAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 16, N'PADRE MARQUEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 16, N'PAMPA HERMOSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 16, N'SARAYACU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 16, N'VARGAS GUERRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 16, N'BARRANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 16, N'CAHUAPANAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 16, N'MANSERICHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 16, N'MORONA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 16, N'PASTAZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 16, N'ANDOAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 17, N'TAMBOPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 17, N'INAMBARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 17, N'LAS PIEDRAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 17, N'LABERINTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 17, N'MANU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 17, N'FITZCARRALD')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 17, N'MADRE DE DIOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 17, N'HUEPETUHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 17, N'IAPARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 17, N'IBERIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 17, N'TAHUAMANU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 18, N'MOQUEGUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 18, N'CARUMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 18, N'CUCHUMBAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 18, N'SAMEGUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 18, N'SAN CRISTOBAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 18, N'TORATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 18, N'OMATE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 18, N'CHOJATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 18, N'COALAQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 18, N'ICHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 18, N'LA CAPILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 18, N'LLOQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 18, N'MATALAQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 18, N'PUQUINA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 2, 18, N'QUINISTAQUILLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 2, 18, N'UBINAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 2, 18, N'YUNGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 18, N'ILO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 18, N'EL ALGARROBAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 18, N'PACOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 19, N'CHAUPIMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 19, N'HUACHON')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 19, N'HUARIACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 19, N'HUAYLLAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 19, N'NINACACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 19, N'PALLANCHACRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 19, N'PAUCARTAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 19, N'SAN FRANCISCO DE ASIS DE YARUSYACAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 19, N'SIMON BOLIVAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 19, N'TICLACAYAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 19, N'TINYAHUARCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 19, N'VICCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 19, N'YANACANCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 19, N'YANAHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 19, N'CHACAYAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 19, N'GOYLLARISQUIZGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 19, N'PAUCAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 19, N'SAN PEDRO DE PILLAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 19, N'SANTA ANA DE TUSI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 19, N'TAPUC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 19, N'VILCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 19, N'OXAPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 19, N'CHONTABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 19, N'HUANCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 19, N'PALCAZU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 19, N'POZUZO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 19, N'PUERTO BERMUDEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 19, N'VILLA RICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 19, N'CONSTITUCION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 20, N'PIURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 20, N'CASTILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 20, N'CATACAOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 20, N'CURA MORI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 20, N'EL TALLAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 20, N'LA ARENA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 20, N'LA UNION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 20, N'LAS LOMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 20, N'TAMBO GRANDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 20, N'AYABACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 20, N'FRIAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 20, N'JILILI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 20, N'LAGUNAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 20, N'MONTERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 20, N'PACAIPAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 20, N'PAIMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 20, N'SAPILLICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 2, 20, N'SICCHEZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 2, 20, N'SUYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 20, N'HUANCABAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 20, N'CANCHAQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 20, N'EL CARMEN DE LA FRONTERA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 20, N'HUARMACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 20, N'LALAQUIZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 20, N'SAN MIGUEL DE EL FAIQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 20, N'SONDOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 20, N'SONDORILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 20, N'CHULUCANAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 20, N'BUENOS AIRES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 20, N'CHALACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 20, N'LA MATANZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 20, N'MORROPON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 20, N'SALITRAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 20, N'SAN JUAN DE BIGOTE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 20, N'SANTA CATALINA DE MOSSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 4, 20, N'SANTO DOMINGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 4, 20, N'YAMANGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 20, N'PAITA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 20, N'AMOTAPE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 20, N'ARENAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 20, N'COLAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 20, N'LA HUACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 20, N'TAMARINDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 20, N'VICHAYAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 20, N'SULLANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 20, N'BELLAVISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 20, N'IGNACIO ESCUDERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 20, N'LANCONES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 20, N'MARCAVELICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 20, N'MIGUEL CHECA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 20, N'QUERECOTILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 20, N'SALITRAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 20, N'PARIAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 20, N'EL ALTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 20, N'LA BREA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 20, N'LOBITOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 20, N'LOS ORGANOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 20, N'MANCORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 20, N'SECHURA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 20, N'BELLAVISTA DE LA UNION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 20, N'BERNAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 20, N'CRISTO NOS VALGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 20, N'VICE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 20, N'RINCONADA LLICUAR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 21, N'PUNO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 21, N'ACORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 21, N'AMANTANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 21, N'ATUNCOLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 21, N'CAPACHICA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 21, N'CHUCUITO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 21, N'COATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 21, N'HUATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 21, N'MAAZO')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 21, N'PAUCARCOLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 1, 21, N'PICHACANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 1, 21, N'PLATERIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 1, 21, N'SAN ANTONIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 1, 21, N'TIQUILLACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 1, 21, N'VILQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 21, N'AZANGARO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 21, N'ACHAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 21, N'ARAPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 21, N'ASILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 21, N'CAMINACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 21, N'CHUPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 2, 21, N'JOSE DOMINGO CHOQUEHUANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 2, 21, N'MUANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 2, 21, N'POTONI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 2, 21, N'SAMAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 2, 21, N'SAN ANTON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 2, 21, N'SAN JOSE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 2, 21, N'SAN JUAN DE SALINAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 2, 21, N'SANTIAGO DE PUPUJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (15, 2, 21, N'TIRAPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 21, N'MACUSANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 21, N'AJOYANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 21, N'AYAPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 21, N'COASA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 21, N'CORANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 3, 21, N'CRUCERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 3, 21, N'ITUATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 3, 21, N'OLLACHEA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 3, 21, N'SAN GABAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 3, 21, N'USICAYOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 21, N'JULI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 21, N'DESAGUADERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 21, N'HUACULLANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 21, N'KELLUYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 21, N'PISACOMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 21, N'POMATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 21, N'ZEPITA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 21, N'ILAVE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 21, N'CAPAZO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 21, N'PILCUYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 21, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 21, N'CONDURIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 21, N'HUANCANE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 21, N'COJATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 21, N'HUATASANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 21, N'INCHUPALLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 21, N'PUSI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 6, 21, N'ROSASPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 21, N'TARACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 21, N'VILQUE CHICO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 21, N'LAMPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 21, N'CABANILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 21, N'CALAPUJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 21, N'NICASIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 21, N'OCUVIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 21, N'PALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 21, N'PARATIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 7, 21, N'PUCARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 7, 21, N'SANTA LUCIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 7, 21, N'VILAVILA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 21, N'AYAVIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 21, N'ANTAUTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 21, N'CUPI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 21, N'LLALLI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 21, N'MACARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 21, N'NUOA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 21, N'ORURILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 21, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 21, N'UMACHIRI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 21, N'MOHO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 21, N'CONIMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 21, N'HUAYRAPATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 21, N'TILALI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 21, N'PUTINA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 21, N'ANANEA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 21, N'PEDRO VILCA APAZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 21, N'QUILCAPUNCU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 21, N'SINA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 11, 21, N'JULIACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 11, 21, N'CABANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 11, 21, N'CABANILLAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 11, 21, N'CARACOTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 12, 21, N'SANDIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 12, 21, N'CUYOCUYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 12, 21, N'LIMBANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 12, 21, N'PATAMBUCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 12, 21, N'PHARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 12, 21, N'QUIACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 12, 21, N'SAN JUAN DEL ORO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 12, 21, N'YANAHUAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 12, 21, N'ALTO INAMBARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 12, 21, N'SAN PEDRO DE PUTINA PUNCO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 13, 21, N'YUNGUYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 13, 21, N'ANAPIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 13, 21, N'COPANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 13, 21, N'CUTURAPI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 13, 21, N'OLLARAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 13, 21, N'TINICACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 13, 21, N'UNICACHI')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 22, N'MOYOBAMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 22, N'CALZADA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 22, N'HABANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 22, N'JEPELACIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 22, N'SORITOR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 22, N'YANTALO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 22, N'BELLAVISTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 22, N'ALTO BIAVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 22, N'BAJO BIAVO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 22, N'HUALLAGA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 22, N'SAN PABLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 22, N'SAN RAFAEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 22, N'SAN JOSE DE SISA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 22, N'AGUA BLANCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 22, N'SAN MARTIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 22, N'SANTA ROSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 3, 22, N'SHATOJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 22, N'SAPOSOA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 22, N'ALTO SAPOSOA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 22, N'EL ESLABON')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 22, N'PISCOYACU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 22, N'SACANCHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 22, N'TINGO DE SAPOSOA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 5, 22, N'LAMAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 5, 22, N'ALONSO DE ALVARADO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 5, 22, N'BARRANQUITA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 5, 22, N'CAYNARACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 5, 22, N'CUUMBUQUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 5, 22, N'PINTO RECODO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 5, 22, N'RUMISAPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 5, 22, N'SAN ROQUE DE CUMBAZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 5, 22, N'SHANAO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 5, 22, N'TABALOSOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 5, 22, N'ZAPATERO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 22, N'JUANJUI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 22, N'CAMPANILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 22, N'HUICUNGO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 6, 22, N'PACHIZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 22, N'PAJARILLO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 7, 22, N'PICOTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 7, 22, N'BUENOS AIRES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 7, 22, N'CASPISAPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 7, 22, N'PILLUANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 7, 22, N'PUCACACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 7, 22, N'SAN CRISTOBAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 7, 22, N'SAN HILARION')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 7, 22, N'SHAMBOYACU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 7, 22, N'TINGO DE PONASA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 7, 22, N'TRES UNIDOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 8, 22, N'RIOJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 8, 22, N'AWAJUN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 8, 22, N'ELIAS SOPLIN VARGAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 8, 22, N'NUEVA CAJAMARCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 8, 22, N'PARDO MIGUEL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 8, 22, N'POSIC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 8, 22, N'SAN FERNANDO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 8, 22, N'YORONGOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 8, 22, N'YURACYACU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 9, 22, N'TARAPOTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 9, 22, N'ALBERTO LEVEAU')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 9, 22, N'CACATACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 9, 22, N'CHAZUTA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 9, 22, N'CHIPURANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 9, 22, N'EL PORVENIR')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 9, 22, N'HUIMBAYOC')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 9, 22, N'JUAN GUERRA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 9, 22, N'LA BANDA DE SHILCAYO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 9, 22, N'MORALES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (11, 9, 22, N'PAPAPLAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (12, 9, 22, N'SAN ANTONIO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (13, 9, 22, N'SAUCE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (14, 9, 22, N'SHAPAJA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 10, 22, N'TOCACHE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 10, 22, N'NUEVO PROGRESO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 10, 22, N'POLVORA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 10, 22, N'SHUNTE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 10, 22, N'UCHIZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 23, N'TACNA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 23, N'ALTO DE LA ALIANZA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 23, N'CALANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 23, N'CIUDAD NUEVA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 23, N'INCLAN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 23, N'PACHIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 23, N'PALCA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 1, 23, N'POCOLLAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 1, 23, N'SAMA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 1, 23, N'CORONEL GREGORIO ALBARRACIN LANCHIPA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 23, N'CANDARAVE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 23, N'CAIRANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 23, N'CAMILACA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 23, N'CURIBAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 2, 23, N'HUANUARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 2, 23, N'QUILAHUANI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 23, N'LOCUMBA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 23, N'ILABAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 23, N'ITE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 23, N'TARATA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 4, 23, N'HEROES ALBARRACIN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 4, 23, N'ESTIQUE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 4, 23, N'ESTIQUE-PAMPA')
GO
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 4, 23, N'SITAJARA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 4, 23, N'SUSAPAYA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 4, 23, N'TARUCACHI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 4, 23, N'TICACO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 24, N'TUMBES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 24, N'CORRALES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 24, N'LA CRUZ')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 24, N'PAMPAS DE HOSPITAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 24, N'SAN JACINTO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 24, N'SAN JUAN DE LA VIRGEN')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 24, N'ZORRITOS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 24, N'CASITAS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 24, N'CANOAS DE PUNTA SAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 24, N'ZARUMILLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 24, N'AGUAS VERDES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 24, N'MATAPALO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 3, 24, N'PAPAYAL')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 1, 25, N'CALLERIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 1, 25, N'CAMPOVERDE')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 1, 25, N'IPARIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 1, 25, N'MASISEA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 1, 25, N'YARINACOCHA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (6, 1, 25, N'NUEVA REQUENA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 1, 25, N'MANANTAY')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 2, 25, N'RAYMONDI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 2, 25, N'SEPAHUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 2, 25, N'TAHUANIA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (4, 2, 25, N'YURUA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 3, 25, N'PADRE ABAD')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 3, 25, N'IRAZOLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 3, 25, N'CURIMANA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 4, 25, N'PURUS')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (16, 1, 5, N'ANDRES AVELINO CACERES')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (1, 6, 12, N'SATIPO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (2, 6, 12, N'COVIRIALI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (3, 6, 12, N'LLAYLLA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (5, 6, 12, N'PAMPA HERMOSA')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (7, 6, 12, N'RIO NEGRO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (8, 6, 12, N'RIO TAMBO')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (10, 6, 12, N'MAZAMARI')
INSERT [dbo].[TBLDISTRITO] ([IdDistrito], [IdProvincia], [IdDepartamento], [NombreDistrito]) VALUES (9, 6, 12, N'PANGOA')
GO
INSERT [dbo].[TBLEMPRESA] ([IdEmpresa], [NombreEmpresa], [LogoEmpresa], [Estado]) VALUES (1, N'IDE SOLUTION', N'imagen.png', 0)
GO
SET IDENTITY_INSERT [dbo].[TBLMODULO] ON 

INSERT [dbo].[TBLMODULO] ([idModulo], [nombre], [Url], [Estado]) VALUES (1, N'VENTAS', N'sales', 0)
INSERT [dbo].[TBLMODULO] ([idModulo], [nombre], [Url], [Estado]) VALUES (2, N'COMPRAS', N'purchases', 0)
INSERT [dbo].[TBLMODULO] ([idModulo], [nombre], [Url], [Estado]) VALUES (3, N'ALMACEN', N'stock', 0)
INSERT [dbo].[TBLMODULO] ([idModulo], [nombre], [Url], [Estado]) VALUES (4, N'REPORTES', NULL, 0)
SET IDENTITY_INSERT [dbo].[TBLMODULO] OFF
GO
INSERT [dbo].[TBLPARAMETRO] ([prefijo], [correlativo], [numero1], [numero2], [fecha1], [fecha2], [decimal1], [decimal2], [texto1], [texto2], [estado]) VALUES (1, 1, NULL, NULL, NULL, NULL, CAST(0.18 AS Decimal(8, 2)), NULL, N'Porcentaje IGV', NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[TBLPERFIL] ON 

INSERT [dbo].[TBLPERFIL] ([idPerfil], [nombre], [Estado]) VALUES (1, N'ADMINISTRADOR', 0)
INSERT [dbo].[TBLPERFIL] ([idPerfil], [nombre], [Estado]) VALUES (2, N'VENDEDOR', 0)
INSERT [dbo].[TBLPERFIL] ([idPerfil], [nombre], [Estado]) VALUES (3, N'COORDINADOR VENTAS', 0)
SET IDENTITY_INSERT [dbo].[TBLPERFIL] OFF
GO
SET IDENTITY_INSERT [dbo].[TBLPERSONA] ON 

INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (1, 1, N'david', 1, N'44444444', N'dvilca@ide-solution.com', N'vilca', N'vilca', 0, NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (2, 2, N'centro de entrenamiento de tecnologia', 2, N'55555555', N'dvilca@ide-solution.com', NULL, NULL, 0, 1, 1, 1, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (3, 1, N'jose', 1, N'66666666', N'dvilca@ide-solution.com', N'baldera', N'baldera', 0, 2, 2, 2, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (9, 1, N'Ernesto Lucas', 1, N'70752620', NULL, N'Suarez', N'Gonzales', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (11, 1, N'Juan Eduardo', 1, N'27360280', NULL, N'Flores', N'Guzman', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (12, 1, N'marco', 1, N'98989898', NULL, N'galvez', N'soria', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (13, 1, N'wilson', 1, N'47073345', NULL, N'vasquez', N'coronado', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (14, 1, N'valeria', 1, N'69696969', NULL, N'vidaurre', N'soria', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (15, 1, N'Ezequiel', 1, N'12122121', NULL, N'Moreno', N'Guzman', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (16, 1, N'WILSON', 1, N'47073344', NULL, N'VASQUEZ', N'VASQUEZ', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (17, 1, N'WILSON', 1, N'47073343', NULL, N'CORONADO', N'CORONADO', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (18, 1, N'EDGAR', 1, N'47073341', NULL, N'VASQUEZ', N'CORONADO', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (19, 1, N'ROSITA', 1, N'47218433', NULL, N'CELIS', N'SOLANO', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (20, 1, N'FLEMING ', 1, N'47396816', NULL, N'ALFARO', N'PARY', 9, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (21, 1, N'JUAN', 1, N'12345678', NULL, N'PEREZ', N'PEREZ', 0, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT [dbo].[TBLPERSONA] ([idPersona], [tipoPersona], [nombre], [tipoDocumento], [numDocumento], [email], [apellidoPaterno], [apellidoMaterno], [estado], [idDepartamento], [idProvincia], [idDistrito], [tipoRegistro], [fechaNacimiento], [numeroContacto], [direccion]) VALUES (22, 1, N'ronald jesus', 1, N'70752618', N'rdiaz@ide-solution.com', N'diaz', N'IDROGO', 0, 6, 4, 1, 2, CAST(N'1993-08-27' AS Date), N'990148740', N'chota')
SET IDENTITY_INSERT [dbo].[TBLPERSONA] OFF
GO
SET IDENTITY_INSERT [dbo].[TBLPRESENTACION] ON 

INSERT [dbo].[TBLPRESENTACION] ([IdPresentacion], [DescripcionPresentacion], [Estado]) VALUES (6, N'unidad', 0)
INSERT [dbo].[TBLPRESENTACION] ([IdPresentacion], [DescripcionPresentacion], [Estado]) VALUES (7, N'kilogramos', 0)
INSERT [dbo].[TBLPRESENTACION] ([IdPresentacion], [DescripcionPresentacion], [Estado]) VALUES (8, N'toneladas', 0)
INSERT [dbo].[TBLPRESENTACION] ([IdPresentacion], [DescripcionPresentacion], [Estado]) VALUES (9, N'unidades', 0)
SET IDENTITY_INSERT [dbo].[TBLPRESENTACION] OFF
GO
SET IDENTITY_INSERT [dbo].[TBLPRODUCTO] ON 

INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (18, N'DSR122', N'Mueble', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (19, N'HHR128', N'Lentes', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (20, N'KUKAF', N'mesa', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (21, N'LOL120', N'caja', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (22, N'EE133', N'GALLETA SODA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (23, N'EE233', N'GALLETA VAINILLA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (24, N'E124', N'LECHE GLORIA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (25, N'EE123', N'GALLETA GLACITAS', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (26, N'EA123', N'AZUCAR BLANCA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (27, N'EE111', N'GALLETA SALTICAS', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (28, N'EE001', N'GALLETA CHOCO SODA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (29, N'PRD001', N'PRODUCTO PRUEBA 1', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (30, N'PRD002', N'PRODUCTO PRUEBA 2', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (31, N'12211111', N'CERAMICA 30X30', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (32, N'1', N'CARGADOR', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (33, N'111', N'BILLETERA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (34, N'EE1112', N'LECHE GLORIA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (35, N'EE333', N'LECHE IDEAL', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (36, N'EE1334', N'GALLETA VAINILLA', 0)
INSERT [dbo].[TBLPRODUCTO] ([IdProducto], [SkuProducto], [DescripcionProducto], [Estado]) VALUES (37, N'LOLI120', N'caja', 0)
SET IDENTITY_INSERT [dbo].[TBLPRODUCTO] OFF
GO
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 1, N'CHACHAPOYAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 1, N'BAGUA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 1, N'BONGARA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 1, N'CONDORCANQUI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 1, N'LUYA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 1, N'RODRIGUEZ DE MENDOZA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 1, N'UTCUBAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 2, N'HUARAZ')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 2, N'AIJA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 2, N'ANTONIO RAYMONDI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 2, N'ASUNCION')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 2, N'BOLOGNESI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 2, N'CARHUAZ')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 2, N'CARLOS FERMIN FITZCARRALD')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 2, N'CASMA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 2, N'CORONGO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 2, N'HUARI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (11, 2, N'HUARMEY')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (12, 2, N'HUAYLAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (13, 2, N'MARISCAL LUZURIAGA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (14, 2, N'OCROS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (15, 2, N'PALLASCA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (16, 2, N'POMABAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (17, 2, N'RECUAY')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (18, 2, N'SANTA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (19, 2, N'SIHUAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (20, 2, N'YUNGAY')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 3, N'ABANCAY')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 3, N'ANDAHUAYLAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 3, N'ANTABAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 3, N'AYMARAES')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 3, N'COTABAMBAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 3, N'CHINCHEROS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 3, N'GRAU')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 4, N'AREQUIPA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 4, N'CAMANA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 4, N'CARAVELI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 4, N'CASTILLA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 4, N'CAYLLOMA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 4, N'CONDESUYOS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 4, N'ISLAY')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 4, N'LA UNION')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 5, N'HUAMANGA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 5, N'CANGALLO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 5, N'HUANCA SANCOS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 5, N'HUANTA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 5, N'LA MAR')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 5, N'LUCANAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 5, N'PARINACOCHAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 5, N'PAUCAR DEL SARA SARA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 5, N'SUCRE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 5, N'VICTOR FAJARDO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (11, 5, N'VILCAS HUAMAN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 6, N'CAJAMARCA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 6, N'CAJABAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 6, N'CELENDIN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 6, N'CHOTA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 6, N'CONTUMAZA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 6, N'CUTERVO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 6, N'HUALGAYOC')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 6, N'JAEN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 6, N'SAN IGNACIO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 6, N'SAN MARCOS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (11, 6, N'SAN MIGUEL')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (12, 6, N'SAN PABLO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (13, 6, N'SANTA CRUZ')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 7, N'CALLAO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 8, N'CUSCO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 8, N'ACOMAYO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 8, N'ANTA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 8, N'CALCA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 8, N'CANAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 8, N'CANCHIS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 8, N'CHUMBIVILCAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 8, N'ESPINAR')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 8, N'LA CONVENCION')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 8, N'PARURO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (11, 8, N'PAUCARTAMBO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (12, 8, N'QUISPICANCHI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (13, 8, N'URUBAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 9, N'HUANCAVELICA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 9, N'ACOBAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 9, N'ANGARAES')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 9, N'CASTROVIRREYNA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 9, N'CHURCAMPA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 9, N'HUAYTARA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 9, N'TAYACAJA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 10, N'HUANUCO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 10, N'AMBO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 10, N'DOS DE MAYO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 10, N'HUACAYBAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 10, N'HUAMALIES')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 10, N'LEONCIO PRADO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 10, N'MARAÑN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 10, N'PACHITEA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 10, N'PUERTO INCA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 10, N'LAURICOCHA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (11, 10, N'YAROWILCA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 11, N'ICA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 11, N'CHINCHA')
GO
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 11, N'NAZCA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 11, N'PALPA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 11, N'PISCO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 12, N'HUANCAYO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 12, N'CONCEPCION')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 12, N'CHANCHAMAYO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 12, N'JAUJA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 12, N'JUNIN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 12, N'SATIPO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 12, N'TARMA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 12, N'YAULI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 12, N'CHUPACA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 13, N'TRUJILLO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 13, N'ASCOPE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 13, N'BOLIVAR')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 13, N'CHEPEN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 13, N'JULCAN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 13, N'OTUZCO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 13, N'PACASMAYO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 13, N'PATAZ')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 13, N'SANCHEZ CARRION')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 13, N'SANTIAGO DE CHUCO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (11, 13, N'GRAN CHIMU')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (12, 13, N'VIRU')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 14, N'CHICLAYO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 14, N'FERREÑAFE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 14, N'LAMBAYEQUE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 15, N'LIMA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 15, N'BARRANCA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 15, N'CAJATAMBO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 15, N'CANTA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 15, N'CAÑETE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 15, N'HUARAL')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 15, N'HUAROCHIRI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 15, N'HUAURA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 15, N'OYON')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 15, N'YAUYOS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 16, N'MAYNAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 16, N'ALTO AMAZONAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 16, N'LORETO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 16, N'MARISCAL RAMON CASTILLA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 16, N'REQUENA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 16, N'UCAYALI')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 16, N'DATEM DEL MARAÑN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 17, N'TAMBOPATA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 17, N'MANU')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 17, N'TAHUAMANU')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 18, N'MARISCAL NIETO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 18, N'GENERAL SANCHEZ CERRO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 18, N'ILO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 19, N'PASCO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 19, N'DANIEL ALCIDES CARRION')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 19, N'OXAPAMPA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 20, N'PIURA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 20, N'AYABACA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 20, N'HUANCABAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 20, N'MORROPON')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 20, N'PAITA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 20, N'SULLANA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 20, N'TALARA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 20, N'SECHURA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 21, N'PUNO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 21, N'AZANGARO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 21, N'CARABAYA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 21, N'CHUCUITO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 21, N'EL COLLAO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 21, N'HUANCANE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 21, N'LAMPA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 21, N'MELGAR')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 21, N'MOHO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 21, N'SAN ANTONIO DE PUTINA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (11, 21, N'SAN ROMAN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (12, 21, N'SANDIA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (13, 21, N'YUNGUYO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 22, N'MOYOBAMBA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 22, N'BELLAVISTA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 22, N'EL DORADO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 22, N'HUALLAGA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (5, 22, N'LAMAS')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (6, 22, N'MARISCAL CACERES')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (7, 22, N'PICOTA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (8, 22, N'RIOJA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (9, 22, N'SAN MARTIN')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (10, 22, N'TOCACHE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 23, N'TACNA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 23, N'CANDARAVE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 23, N'JORGE BASADRE')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 23, N'TARATA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 24, N'TUMBES')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 24, N'CONTRALMIRANTE VILLAR')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 24, N'ZARUMILLA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (1, 25, N'CORONEL PORTILLO')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (2, 25, N'ATALAYA')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (3, 25, N'PADRE ABAD')
INSERT [dbo].[TBLPROVINCIA] ([IdProvincia], [IdDepartamento], [NombreProvincia]) VALUES (4, 25, N'PURUS')
GO
SET IDENTITY_INSERT [dbo].[TBLRELACION_P_M] ON 

INSERT [dbo].[TBLRELACION_P_M] ([idPerfilModulo], [idPerfil], [idUsuario], [idModulo], [idSubmodulo]) VALUES (1, 1, 9999, 9999, 9999)
INSERT [dbo].[TBLRELACION_P_M] ([idPerfilModulo], [idPerfil], [idUsuario], [idModulo], [idSubmodulo]) VALUES (2, 2, 9999, 1, 9999)
INSERT [dbo].[TBLRELACION_P_M] ([idPerfilModulo], [idPerfil], [idUsuario], [idModulo], [idSubmodulo]) VALUES (5, 3, 9999, 4, 1)
SET IDENTITY_INSERT [dbo].[TBLRELACION_P_M] OFF
GO
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (18, 6)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (19, 6)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (20, 7)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (21, 8)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (24, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (25, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (26, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (27, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (28, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (34, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (35, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (36, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (37, 8)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (22, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (23, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (29, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (30, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (31, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (32, 9)
INSERT [dbo].[TBLRELACION_P_P] ([IdProducto], [IdPresentacion]) VALUES (33, 9)
GO
INSERT [dbo].[TBLRELACION_U_S] ([IdUsuario_rel], [IdSucursal_rel]) VALUES (1, 1)
INSERT [dbo].[TBLRELACION_U_S] ([IdUsuario_rel], [IdSucursal_rel]) VALUES (1, 2)
INSERT [dbo].[TBLRELACION_U_S] ([IdUsuario_rel], [IdSucursal_rel]) VALUES (2, 3)
GO
SET IDENTITY_INSERT [dbo].[TBLSUBMODULO] ON 

INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (1, 1, N'FACTURACION', N'addventa', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (2, 1, N'PREVENTA', N'addpreventa', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (3, 1, N'NOTA DE CREDITO', N'addnotacredito', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (4, 2, N'PEDIDO', N'addpedido', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (5, 2, N'INGRESO MERCADERIA', N'addigreso', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (6, 2, N'EGRESO MERCADERIA', N'addegreso', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (7, 3, N'PRODUCTOS', N'addproducto', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (8, 3, N'PRECIOS', N'addprecio', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (9, 3, N'STOCK', N'addstock', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (10, 3, N'UNIDADES', N'addunidades', 0)
INSERT [dbo].[TBLSUBMODULO] ([idSubmodulo], [idModulo], [nombre], [Url], [estado]) VALUES (11, 4, N'REPORTE VENTAS', NULL, 0)
SET IDENTITY_INSERT [dbo].[TBLSUBMODULO] OFF
GO
SET IDENTITY_INSERT [dbo].[TBLSUCURSAL] ON 

INSERT [dbo].[TBLSUCURSAL] ([idSucursal], [razonSocial], [tipoDocumento], [numDocumento], [direccion], [telefono], [email], [representante], [IdEmpresa], [estado]) VALUES (1, N'Agencia Chiclayo', 2, N'2012345642', N'', N'999999999', N'guesa@hotmail.com', N'daniel zapata', 1, 0)
INSERT [dbo].[TBLSUCURSAL] ([idSucursal], [razonSocial], [tipoDocumento], [numDocumento], [direccion], [telefono], [email], [representante], [IdEmpresa], [estado]) VALUES (2, N'Agencia Piura', 2, N'201231234642', N'', N'999999999', N'guesa@hotmail.com', N'daniel zalasar', 1, 0)
INSERT [dbo].[TBLSUCURSAL] ([idSucursal], [razonSocial], [tipoDocumento], [numDocumento], [direccion], [telefono], [email], [representante], [IdEmpresa], [estado]) VALUES (3, N'guesa 2 eirl', 2, N'201237654342', N'', N'999999999', N'guesa@hotmail.com', N'daniel zapata', 1, 0)
SET IDENTITY_INSERT [dbo].[TBLSUCURSAL] OFF
GO
SET IDENTITY_INSERT [dbo].[TBLUSUARIO] ON 

INSERT [dbo].[TBLUSUARIO] ([idUsuario], [idPerfil], [documento], [cuenta], [clave], [sal], [fechaRegistro], [IdPersona], [estado]) VALUES (1, 1, N'11111111', N'administrador@empresa.com', N'Eo08WVlogXfTNjhnXZb5WWWd5ATuhajAIhK2mrqQ+cQ=', N'u42eJKL/VDb2c0IJb73Apw==', CAST(N'2021-02-05' AS Date), 1, 0)
INSERT [dbo].[TBLUSUARIO] ([idUsuario], [idPerfil], [documento], [cuenta], [clave], [sal], [fechaRegistro], [IdPersona], [estado]) VALUES (2, 2, N'22222222', N'FALFARO', N'9wgzz+5+j/t8RfqDHmYAV6Ud83geb81047mWsgjGPSk=', N'2yoT7tzB8cnmCqT6EGlQRw==', CAST(N'2021-02-05' AS Date), 1, 0)
INSERT [dbo].[TBLUSUARIO] ([idUsuario], [idPerfil], [documento], [cuenta], [clave], [sal], [fechaRegistro], [IdPersona], [estado]) VALUES (3, 3, N'33333333', N'JCHUMIOQUE', N'P931zB7Uv9vIGSv/WXCa7tdzN169+Vu1EIOqe2yR8nM=', N'J3mG5TB9ezwX5IKnpUCL2A==', CAST(N'2021-02-05' AS Date), 1, 0)
INSERT [dbo].[TBLUSUARIO] ([idUsuario], [idPerfil], [documento], [cuenta], [clave], [sal], [fechaRegistro], [IdPersona], [estado]) VALUES (4, 2, N'44444444', N'JGALAN', N'ioMNMKWkY+arXR8b9N3H/s8RitDGfi+byhE5qlk69J0=', N'+oN/di7f0cS23xdjhFO5Xw==', CAST(N'2021-02-05' AS Date), 2, 0)
INSERT [dbo].[TBLUSUARIO] ([idUsuario], [idPerfil], [documento], [cuenta], [clave], [sal], [fechaRegistro], [IdPersona], [estado]) VALUES (5, 2, N'88888888', N'jj@empresa.com', N'bqZxOOBbxZAzeTxLGxWUbe4YuBHTsZTmCmEHVKvFeU8=', N'ZOXvf2PUJIm6UPHIgjngtQ==', CAST(N'2008-05-01' AS Date), 2, 0)
SET IDENTITY_INSERT [dbo].[TBLUSUARIO] OFF
GO
SET IDENTITY_INSERT [dbo].[TBLVENTA] ON 

INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (69, CAST(N'2021-03-12T12:00:00.000' AS DateTime), N'47073345', NULL, 1, 1, CAST(14.16 AS Decimal(10, 2)), 1, 0, CAST(12.00 AS Decimal(10, 2)), CAST(2.16 AS Decimal(10, 2)), 1)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (70, CAST(N'2021-03-13T12:00:00.000' AS DateTime), N'47073345', NULL, 1, 1, CAST(25.96 AS Decimal(10, 2)), 1, 0, CAST(22.00 AS Decimal(10, 2)), CAST(3.96 AS Decimal(10, 2)), 2)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (71, CAST(N'2021-03-10T12:00:00.000' AS DateTime), N'47218433', NULL, 1, 1, CAST(3.54 AS Decimal(10, 2)), 1, 0, CAST(3.00 AS Decimal(10, 2)), CAST(0.54 AS Decimal(10, 2)), 2)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (74, CAST(N'2021-03-16T15:06:19.000' AS DateTime), N'47073345', NULL, 1, 1, CAST(2.36 AS Decimal(10, 2)), 1, 0, CAST(2.00 AS Decimal(10, 2)), CAST(0.36 AS Decimal(10, 2)), 2)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (75, CAST(N'2021-03-15T02:17:25.000' AS DateTime), N'47218433', NULL, 1, 1, CAST(183.49 AS Decimal(10, 2)), 150, 0, CAST(155.50 AS Decimal(10, 2)), CAST(27.99 AS Decimal(10, 2)), 1)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (76, CAST(N'2021-03-04T00:00:00.000' AS DateTime), N'69696969', NULL, 80, 1, CAST(100.00 AS Decimal(10, 2)), 2, 0, CAST(100.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (77, CAST(N'2021-03-04T00:00:00.000' AS DateTime), N'69696969', NULL, 80, 1, CAST(118.00 AS Decimal(10, 2)), 2, 0, CAST(100.00 AS Decimal(10, 2)), CAST(18.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (78, CAST(N'2021-03-16T01:31:07.000' AS DateTime), N'47073345', NULL, 1, 1, CAST(14.16 AS Decimal(10, 2)), 1, 0, CAST(12.00 AS Decimal(10, 2)), CAST(2.16 AS Decimal(10, 2)), 1)
INSERT [dbo].[TBLVENTA] ([IdVenta], [FechaCreacion], [DocumentoCliente], [FechaActualizacion], [IdSucursal], [IdUsuario], [MontoTotal], [TotalArticulos], [Estado], [Subtotal], [MontoIGV], [EstadoPago]) VALUES (82, CAST(N'2021-03-16T02:15:35.000' AS DateTime), N'47073345', NULL, 1, 1, CAST(413.00 AS Decimal(10, 2)), 100, 0, CAST(350.00 AS Decimal(10, 2)), CAST(63.00 AS Decimal(10, 2)), 1)
SET IDENTITY_INSERT [dbo].[TBLVENTA] OFF
GO
SET IDENTITY_INSERT [dbo].[TBLVENTADETALLE] ON 

INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (149, 69, N'EE133', CAST(12.00 AS Decimal(10, 2)), 1, CAST(12.00 AS Decimal(10, 2)), N'unidades', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (150, 70, N'EE111', CAST(22.00 AS Decimal(10, 2)), 1, CAST(22.00 AS Decimal(10, 2)), N'unidades', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (151, 71, N'EE1112', CAST(3.00 AS Decimal(10, 2)), 1, CAST(3.00 AS Decimal(10, 2)), N'unidades', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (152, 74, N'EE133', CAST(2.00 AS Decimal(10, 2)), 1, CAST(2.00 AS Decimal(10, 2)), N'unidades', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (161, 76, N'KUKAF', CAST(10.00 AS Decimal(10, 2)), 1, CAST(10.00 AS Decimal(10, 2)), N'kilogramos', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (164, 77, N'KUKAF', CAST(10.00 AS Decimal(10, 2)), 1, CAST(10.00 AS Decimal(10, 2)), N'kilogramos', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (165, 78, N'EE133', CAST(12.00 AS Decimal(10, 2)), 1, CAST(12.00 AS Decimal(10, 2)), N'unidades', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (175, 82, N'EE133', CAST(3.50 AS Decimal(10, 2)), 100, CAST(350.00 AS Decimal(10, 2)), N'unidades', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (179, 75, N'EE133', CAST(0.40 AS Decimal(10, 2)), 100, CAST(40.00 AS Decimal(10, 2)), N'unidades', 1)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (180, 75, N'EE111', CAST(0.85 AS Decimal(10, 2)), 30, CAST(25.50 AS Decimal(10, 2)), N'unidades', 3)
INSERT [dbo].[TBLVENTADETALLE] ([IdDetalleVenta], [IdVenta], [SkuProducto], [PrecioProducto], [CantidadProducto], [ImporteProducto], [Presentacion], [Item]) VALUES (181, 75, N'EE1112', CAST(4.50 AS Decimal(10, 2)), 20, CAST(90.00 AS Decimal(10, 2)), N'unidades', 2)
SET IDENTITY_INSERT [dbo].[TBLVENTADETALLE] OFF
GO
ALTER TABLE [dbo].[TBLCOMPROBANTE] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLEMPRESA] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLMODULO] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLPAGOEFECTIVO] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLPAGOTRANSFERENCIA] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLPARAMETRO] ADD  DEFAULT ((0)) FOR [estado]
GO
ALTER TABLE [dbo].[TBLPERFIL] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLPERSONA] ADD  DEFAULT ((0)) FOR [estado]
GO
ALTER TABLE [dbo].[TBLPRESENTACION] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLPRODUCTO] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLSUBMODULO] ADD  DEFAULT ((0)) FOR [estado]
GO
ALTER TABLE [dbo].[TBLSUCURSAL] ADD  DEFAULT ((0)) FOR [estado]
GO
ALTER TABLE [dbo].[TBLUSUARIO] ADD  DEFAULT ((0)) FOR [estado]
GO
ALTER TABLE [dbo].[TBLVENTA] ADD  DEFAULT ((0)) FOR [Estado]
GO
ALTER TABLE [dbo].[TBLVENTA] ADD  DEFAULT ((1)) FOR [EstadoPago]
GO
ALTER TABLE [dbo].[TBLCOMPROBANTE]  WITH CHECK ADD FOREIGN KEY([IdSucursal])
REFERENCES [dbo].[TBLSUCURSAL] ([idSucursal])
GO
ALTER TABLE [dbo].[TBLPAGOEFECTIVO]  WITH CHECK ADD FOREIGN KEY([IdVenta])
REFERENCES [dbo].[TBLVENTA] ([IdVenta])
GO
ALTER TABLE [dbo].[TBLPAGOTRANSFERENCIA]  WITH CHECK ADD FOREIGN KEY([IdVenta])
REFERENCES [dbo].[TBLVENTA] ([IdVenta])
GO
ALTER TABLE [dbo].[TBLRELACION_P_P]  WITH CHECK ADD  CONSTRAINT [FK_Presentacion] FOREIGN KEY([IdPresentacion])
REFERENCES [dbo].[TBLPRESENTACION] ([IdPresentacion])
GO
ALTER TABLE [dbo].[TBLRELACION_P_P] CHECK CONSTRAINT [FK_Presentacion]
GO
ALTER TABLE [dbo].[TBLRELACION_P_P]  WITH CHECK ADD  CONSTRAINT [FK_Producto] FOREIGN KEY([IdProducto])
REFERENCES [dbo].[TBLPRODUCTO] ([IdProducto])
GO
ALTER TABLE [dbo].[TBLRELACION_P_P] CHECK CONSTRAINT [FK_Producto]
GO
ALTER TABLE [dbo].[TBLRELACION_U_S]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal] FOREIGN KEY([IdUsuario_rel])
REFERENCES [dbo].[TBLSUCURSAL] ([idSucursal])
GO
ALTER TABLE [dbo].[TBLRELACION_U_S] CHECK CONSTRAINT [FK_Sucursal]
GO
ALTER TABLE [dbo].[TBLRELACION_U_S]  WITH CHECK ADD  CONSTRAINT [FK_Usuario] FOREIGN KEY([IdSucursal_rel])
REFERENCES [dbo].[TBLUSUARIO] ([idUsuario])
GO
ALTER TABLE [dbo].[TBLRELACION_U_S] CHECK CONSTRAINT [FK_Usuario]
GO
ALTER TABLE [dbo].[TBLSUCURSAL]  WITH CHECK ADD  CONSTRAINT [FK_Empresa] FOREIGN KEY([IdEmpresa])
REFERENCES [dbo].[TBLEMPRESA] ([IdEmpresa])
GO
ALTER TABLE [dbo].[TBLSUCURSAL] CHECK CONSTRAINT [FK_Empresa]
GO
ALTER TABLE [dbo].[TBLUSUARIO]  WITH CHECK ADD  CONSTRAINT [FK_Persona] FOREIGN KEY([IdPersona])
REFERENCES [dbo].[TBLPERSONA] ([idPersona])
GO
ALTER TABLE [dbo].[TBLUSUARIO] CHECK CONSTRAINT [FK_Persona]
GO
ALTER TABLE [dbo].[TBLVENTADETALLE]  WITH CHECK ADD  CONSTRAINT [FK_Venta] FOREIGN KEY([IdVenta])
REFERENCES [dbo].[TBLVENTA] ([IdVenta])
GO
ALTER TABLE [dbo].[TBLVENTADETALLE] CHECK CONSTRAINT [FK_Venta]
GO
/****** Object:  StoredProcedure [dbo].[listaacceso]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[listaacceso]
AS 
BEGIN 
SET NOCOUNT ON; 
select * from acceso
end
GO
/****** Object:  StoredProcedure [dbo].[pa_editar_cliente]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_editar_cliente]
    @Cliente XML,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SET NOCOUNT ON;

    DECLARE        
        @StatusTran INT = 0,
        @CustomerId INT = 0,
        @Names VARCHAR(100),
        @LastName VARCHAR(50),
        @MotherLastName VARCHAR(50),
        @Email VARCHAR(50),
        @DateBirth DATE,
        @ContactNumber VARCHAR(10),
        @Address VARCHAR(300),
        @DepartmentId INT,
        @ProvinceId INT,
        @DistrictId INT

    BEGIN TRAN Tupd

    BEGIN TRY

        SET @CustomerId = @Cliente.value('AddUpdCustomerRequest[1]/CodigoCliente[1]', 'INT');
        SET @Names = @Cliente.value('AddUpdCustomerRequest[1]/Nombres[1]', 'VARCHAR(100)');
        SET @LastName = @Cliente.value('AddUpdCustomerRequest[1]/ApellidoPaterno[1]', 'VARCHAR(50)');
        SET @MotherLastName = @Cliente.value('AddUpdCustomerRequest[1]/ApellidoMaterno[1]', 'VARCHAR(50)');
        SET @Email = @Cliente.value('AddUpdCustomerRequest[1]/Correo[1]', 'VARCHAR(50)');
        SET @DateBirth = @Cliente.value('AddUpdCustomerRequest[1]/FechaNacimiento[1]', 'DATE');
        SET @ContactNumber = @Cliente.value('AddUpdCustomerRequest[1]/NumeroContacto[1]', 'VARCHAR(10)');
        SET @Address = @Cliente.value('AddUpdCustomerRequest[1]/Direccion[1]', 'VARCHAR(300)');
        SET @DepartmentId = @Cliente.value('AddUpdCustomerRequest[1]/CodigoDepartamento[1]', 'INT');
        SET @ProvinceId = @Cliente.value('AddUpdCustomerRequest[1]/CodigoProvincia[1]', 'INT');
        SET @DistrictId = @Cliente.value('AddUpdCustomerRequest[1]/CodigoDistrito[1]', 'INT');
        
        UPDATE TBLPERSONA SET
            nombre = @Names,
            apellidoPaterno = @LastName,
            apellidoMaterno = @MotherLastName,
            email = @Email,
            FechaNacimiento = @DateBirth,
            numeroContacto = @ContactNumber,
            direccion = @Address,
            idDepartamento = @DepartmentId,
            idProvincia = @ProvinceId,
            idDistrito = @DistrictId
        WHERE idPersona = @CustomerId

        IF @@ERROR <> 0 SET @StatusTran = 1        

        IF @StatusTran = 1
            ROLLBACK TRAN Tupd
        ELSE
            SET @Codigo = @CustomerId
            SET @Mensaje = 'Actualización OK'
            COMMIT TRAN Tupd

    END TRY

    BEGIN CATCH

        SET @Codigo = -1
        SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
        ROLLBACK TRAN Tupd

    END CATCH
GO
/****** Object:  StoredProcedure [dbo].[pa_editar_detalle_venta_v1]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_editar_detalle_venta_v1]
    @Venta XML,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SET NOCOUNT ON;

    DECLARE 
        @SaleId INT = 0,
        @StatusTran INT = 0,
        @QuantityProductsBD INT = 0,
        @QuantityProducts INT = 0,
        @SKU NVARCHAR(50),
        @Price DECIMAL(10,2),
        @Quantity INT,
        @Amount DECIMAL(10,2),
        @Presentation NVARCHAR(50),
        @Item INT

    BEGIN TRAN Tupd

    BEGIN TRY
        SET @SaleId = @Venta.value('AddUpdSaleRequest[1]/CodigoVenta[1]', 'INT');
        SET @QuantityProducts = @Venta.value('AddUpdSaleRequest[1]/NumeroArticulos[1]', 'INT');
        SELECT @QuantityProductsBD = TotalArticulos FROM TBLVENTA WHERE IdVenta = @SaleId

        DECLARE SaleDetailList CURSOR LOCAL FOR
            SELECT
                t.p.value('(Producto/SKU)[1]', 'NVARCHAR(50)'),
                t.p.value('(Precio)[1]', 'DECIMAL(10,2)'),
                t.p.value('(Cantidad)[1]', 'INT'),
                t.p.value('(Importe)[1]', 'DECIMAL(10,2)'),
                t.p.value('(Presentacion)[1]', 'NVARCHAR(50)'),
                t.p.value('(Item)[1]', 'INT')
            FROM @Venta.nodes('AddUpdSaleRequest/DetalleVenta/SaleDetail') t(p)
        
        OPEN SaleDetailList

        FETCH NEXT FROM SaleDetailList INTO @SKU, @Price, @Quantity, @Amount, @Presentation, @Item

        IF @QuantityProducts >= @QuantityProductsBD
            BEGIN
                
                WHILE (@@FETCH_STATUS = 0)
                    BEGIN

                        IF EXISTS (SELECT * FROM TBLVENTADETALLE WHERE IdVenta = @SaleId AND SkuProducto = @SKU)
                            BEGIN
                                UPDATE TBLVENTADETALLE SET 
                                    PrecioProducto = @Price,
                                    CantidadProducto = @Quantity,
                                    ImporteProducto = @Amount,
                                    Presentacion = @Presentation
                                WHERE IdVenta = @SaleId AND SkuProducto = @SKU
                            END
                        ELSE
                            BEGIN
                                INSERT INTO TBLVENTADETALLE --(IdVenta, SkuProducto, PrecioProducto, CantidadProducto, ImporteProducto, Presentacion, Item)
                                VALUES (@SaleId, @SKU, @Price, @Quantity, @Amount, @Presentation, @Item)
                            END

                        EXEC pa_registrar_producto @Venta, @StatusTran OUT

                        IF @@ERROR <> 0 SET @StatusTran = 1 
                        IF @StatusTran = 1 BREAK

                        FETCH NEXT FROM SaleDetailList INTO @SKU, @Price, @Quantity, @Amount, @Presentation, @Item
                    END
            END

        ELSE

            BEGIN

                WHILE (@@FETCH_STATUS = 0)
                    BEGIN
                        DELETE FROM TBLVENTADETALLE WHERE IdVenta = @SaleId
                        
                        INSERT INTO TBLVENTADETALLE --(IdVenta, SkuProducto, PrecioProducto, CantidadProducto, ImporteProducto, Presentacion, Item)
                        VALUES (@SaleId, @SKU, @Price, @Quantity, @Amount, @Presentation, @Item)                            

                        EXEC pa_registrar_producto @Venta, @StatusTran OUT

                        IF @@ERROR <> 0 SET @StatusTran = 1 
                        IF @StatusTran = 1 BREAK

                        FETCH NEXT FROM SaleDetailList INTO @SKU, @Price, @Quantity, @Amount, @Presentation, @Item
                    END
            END            
        

        IF @StatusTran = 1
            ROLLBACK TRAN Tupd
        ELSE
            SET @Mensaje = 'Registro OK'
            COMMIT TRAN Tupd

    END TRY

    BEGIN CATCH

        SET @Codigo = -1
        SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
        ROLLBACK TRAN Tupd

    END CATCH
GO
/****** Object:  StoredProcedure [dbo].[pa_editar_venta]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_editar_venta]
    @Venta XML,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SET NOCOUNT ON;

    DECLARE 
        @SaleId INT = 0,
        @StatusTran INT = 0,
        @QuantityProducts INT = 0,
        @Subtotal DECIMAL(10,2),
        @TotaAmount DECIMAL(10,2),
        @IGVAmount DECIMAL(10,2),
        @SKU NVARCHAR(50),
        @Price DECIMAL(10,2),
        @Quantity INT,
        @Amount DECIMAL(10,2),
        @Presentation NVARCHAR(50),
        @Item INT

    BEGIN TRAN Tupd

    BEGIN TRY
        SET @SaleId = @Venta.value('AddUpdSaleRequest[1]/CodigoVenta[1]', 'INT');
        SET @QuantityProducts = @Venta.value('AddUpdSaleRequest[1]/NumeroArticulos[1]', 'INT');
        SET @Subtotal = @Venta.value('AddUpdSaleRequest[1]/Subtotal[1]', 'DECIMAL(10,2)');
        SET @TotaAmount = @Venta.value('AddUpdSaleRequest[1]/MontoTotal[1]', 'DECIMAL(10,2)');
        SET @IGVAmount = @Venta.value('AddUpdSaleRequest[1]/MontoIGV[1]', 'DECIMAL(10,2)');

        DELETE FROM TBLVENTADETALLE WHERE IdVenta = @SaleId

        IF @@ERROR <> 0 
            SET @StatusTran = 1 

        ELSE
            BEGIN
                INSERT TBLVENTADETALLE
                SELECT @SaleId, 
                    t.p.value('(Producto/SKU)[1]', 'NVARCHAR(50)'),
                    t.p.value('(Precio)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(Cantidad)[1]', 'INT'),
                    t.p.value('(Importe)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(Presentacion)[1]', 'NVARCHAR(50)'),
                    t.p.value('(Item)[1]', 'INT')
                FROM @Venta.nodes('AddUpdSaleRequest/DetalleVenta/SaleDetail') t(p)

                IF @@ERROR <> 0 
                    SET @StatusTran = 1

                ELSE                             
                    EXEC pa_registrar_producto @Venta, @StatusTran OUT

                    IF @StatusTran <> 1
                        UPDATE TBLVENTA SET
                            TotalArticulos = @QuantityProducts,
                            Subtotal = @Subtotal,
                            MontoTotal = @TotaAmount,
                            MontoIGV = @IGVAmount
                        WHERE IdVenta = @SaleId

                        IF @@ERROR <> 0 SET @StatusTran = 1
            END       

        IF @StatusTran = 1
            ROLLBACK TRAN Tupd
        ELSE
            SET @Codigo = @SaleId
            SET @Mensaje = 'Actualización OK'
            COMMIT TRAN Tupd

    END TRY

    BEGIN CATCH

        SET @Codigo = -1
        SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
        ROLLBACK TRAN Tupd

    END CATCH

GO
/****** Object:  StoredProcedure [dbo].[pa_eliminar_cliente]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_eliminar_cliente]
    @CodigoCliente INT,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT
-- add more stored procedure parameters here
AS
    SET NOCOUNT ON;
    
    DECLARE        
        @StatusTran INT = 0

    BEGIN TRAN Tdel

    BEGIN TRY

        UPDATE TBLPERSONA SET Estado = 9 WHERE idPersona = @CodigoCliente

        IF @@ERROR <> 0 SET @StatusTran = 1        

        IF @StatusTran = 1
            ROLLBACK TRAN Tdel
        ELSE
            SET @Codigo = @CodigoCliente
            SET @Mensaje = 'Eliminar OK'
            COMMIT TRAN Tdel

    END TRY

    BEGIN CATCH

        SET @Codigo = -1
        SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
        ROLLBACK TRAN Tdel

    END CATCH    

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_accesos_modulos]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_accesos_modulos]
    @IdPerfil int,   
    @IdUsuario int
-- add more stored procedure parameters here
AS
    BEGIN
        SET NOCOUNT ON;
        
        DECLARE 
            @usuario int,
            @modulo int,
            @submodulo int

    SET NOCOUNT ON;

        SELECT @modulo=idModulo, @submodulo=idSubmodulo FROM tblrelacion_p_m WHERE idperfil= @IdPerfil

        IF @modulo=9999
            IF @submodulo=9999
                BEGIN
                    SELECT TBLMODULO.idModulo, TBLMODULO.nombre, TBLMODULO.Url, TBLSUBMODULO.idSubmodulo, TBLSUBMODULO.nombre AS nombre_sub, TBLSUBMODULO.Url AS Url_sub
                    FROM TBLSUBMODULO
                    INNER JOIN TBLMODULO
                    ON TBLSUBMODULO.idModulo = TBLMODULO.IdModulo
                END
        ELSE
            BEGIN
                IF @submodulo=9999
                    BEGIN
                        SELECT TBLMODULO.idModulo, TBLMODULO.nombre, TBLMODULO.Url, TBLSUBMODULO.idSubmodulo, TBLSUBMODULO.nombre AS nombre_sub, TBLSUBMODULO.Url AS Url_sub
                        FROM TBLSUBMODULO
                        INNER JOIN TBLMODULO
                        ON TBLSUBMODULO.idModulo = TBLMODULO.IdModulo
                        WHERE TBLMODULO.idModulo = @modulo
                    END
                ELSE
                    BEGIN
                        SELECT TBLMODULO.idModulo, TBLMODULO.nombre, TBLMODULO.Url, TBLSUBMODULO.idSubmodulo, TBLSUBMODULO.nombre AS nombre_sub, TBLSUBMODULO.Url AS Url_sub
                        FROM TBLSUBMODULO
                        INNER JOIN TBLMODULO
                        ON TBLSUBMODULO.idModulo = TBLMODULO.IdModulo
                        WHERE TBLMODULO.idModulo = @modulo
                        AND TBLSUBMODULO.idSubmodulo = @submodulo
                    END
            END
    END
GO
/****** Object:  StoredProcedure [dbo].[pa_listar_agencias]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_agencias]

AS
    -- body of the stored procedure
    SELECT idSucursal, razonSocial
    FROM TBLSUCURSAL

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_agencias_usuario]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_agencias_usuario]
    @codigo_usuario /*parameter name*/ int /*datatype_for_param1*/ = 0 /*default_value_for_param2*/
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SELECT IdSucursal_rel, razonSocial
    FROM TBLRELACION_U_S
    INNER JOIN TBLSUCURSAL
    ON TBLRELACION_U_S.IdSucursal_rel = TBLSUCURSAL.idSucursal
    INNER JOIN TBLUSUARIO
    ON TBLRELACION_U_S.IdUsuario_rel = TBLUSUARIO.idUsuario
    WHERE TBLUSUARIO.idUsuario = @codigo_usuario
    

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_clientes]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_clientes]
    @NumeroDocumento VARCHAR(15),
    @DatosCliente VARCHAR(100),
    @TipoPersona INT
AS
    SET NOCOUNT ON;

    DECLARE
        @sqlQuery NVARCHAR(4000)
    
    SET @sqlQuery = 
    '
        SELECT DISTINCT 
            idPersona, tipoPersona, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento, TBLPERSONA.numDocumento, fechaNacimiento, numeroContacto, email, direccion,
            descripcion AS descripcionTipoPersona, 
            NombreDepartamento, 
            NombreProvincia,
            NombreDistrito
        FROM TBLPERSONA
        INNER JOIN TBLCONCEPTO
        ON TBLPERSONA.tipoPersona = TBLCONCEPTO.correlativo AND TBLCONCEPTO.prefijo = 4
        LEFT JOIN TBLDEPARTAMENTO
        ON TBLPERSONA.idDepartamento = TBLDEPARTAMENTO.IdDepartamento
        LEFT JOIN TBLPROVINCIA
        ON TBLPERSONA.idProvincia = TBLPROVINCIA.IdProvincia AND TBLPERSONA.idDepartamento = TBLPROVINCIA.IdDepartamento
        LEFT JOIN TBLDISTRITO
        ON TBLPERSONA.idDistrito = TBLDISTRITO.IdDistrito AND TBLPERSONA.idProvincia = TBLDISTRITO.IdProvincia AND TBLPERSONA.idDepartamento = TBLDISTRITO.IdDepartamento
        INNER JOIN v_DatosCliente 
        ON TBLPERSONA.numDocumento = v_DatosCliente.numDocumento
        WHERE tipoRegistro = 2 AND estado = 0
    '

    IF @NumeroDocumento != ''
        SET @sqlQuery = CONCAT(@sqlQuery, ' AND TBLPERSONA.numDocumento = ', @NumeroDocumento)

    IF @DatosCliente != ''
        SET @sqlQuery = CONCAT(@sqlQuery, ' AND DatosCliente LIKE ''%', @DatosCliente, '%''')

    IF @TipoPersona > 0
        SET @sqlQuery = CONCAT(@sqlQuery, ' AND tipoPersona = ', @TipoPersona)

    EXEC (@sqlQuery);
    


GO
/****** Object:  StoredProcedure [dbo].[pa_listar_conceptos]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_conceptos]
    @Flag int,
    @Prefijo int,
    @Correlativo int
-- add more stored procedure parameters here
AS
    --Correlativo y descripcion a partir de prefijo
    IF @Flag = 1
        SELECT correlativo, descripcion FROM TBLCONCEPTO WHERE prefijo = @Prefijo AND correlativo > 0
GO
/****** Object:  StoredProcedure [dbo].[pa_listar_datos_cliente]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_datos_cliente]
    @numero_documento VARCHAR(15)
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SELECT tipoDocumento, nombre, apellidoPaterno, apellidoMaterno
    FROM TBLPERSONA
    WHERE numDocumento = @numero_documento
    AND tipoRegistro = 2


GO
/****** Object:  StoredProcedure [dbo].[pa_listar_datos_empresa]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_datos_empresa]
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SELECT TOP 1 IdEmpresa, NombreEmpresa, LogoEmpresa
    FROM TBLEMPRESA

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_datos_producto]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_datos_producto]
    @sku /*parameter name*/ NVARCHAR(50)
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SELECT DescripcionProducto, DescripcionPresentacion
    FROM TBLPRODUCTO
    INNER JOIN TBLRELACION_P_P
    ON TBLPRODUCTO.IdProducto = TBLRELACION_P_P.IdProducto
    INNER JOIN TBLPRESENTACION
    ON TBLPRESENTACION.IdPresentacion = TBLRELACION_P_P.IdPresentacion
    WHERE SkuProducto = @sku

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_datos_usuario]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_datos_usuario]
    @codigo_usuario /*parameter name*/ int /*datatype_for_param1*/ = 0 /*default_value_for_param2*/
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SELECT nombre, apellidoPaterno, apellidoMaterno
    FROM TBLPERSONA
    INNER JOIN TBLUSUARIO
    ON TBLPERSONA.idPersona = TBLUSUARIO.IdPersona
    WHERE TBLUSUARIO.idUsuario = @codigo_usuario

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_detalle_venta]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_detalle_venta]
    @id_venta INT
AS
    -- body of the stored procedure
    SELECT Item, TBLVENTADETALLE.SkuProducto, DescripcionProducto, CantidadProducto, Presentacion, PrecioProducto, ImporteProducto
    FROM TBLVENTADETALLE
    INNER JOIN TBLPRODUCTO
    ON TBLVENTADETALLE.SkuProducto = TBLPRODUCTO.SkuProducto
    WHERE IdVenta = @id_venta
    ORDER BY 1



GO
/****** Object:  StoredProcedure [dbo].[pa_listar_parametros]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_parametros]
    @flag int,
    @prefijo int,
    @correlativo int
-- add more stored procedure parameters here
AS
    --Parametros Generales
    IF @flag = 1
        --IGV
        IF @correlativo = 1 SELECT decimal1 AS IGV FROM TBLPARAMETRO WHERE prefijo = @prefijo AND correlativo = @correlativo

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_presentaciones_producto]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_presentaciones_producto]
-- add more stored procedure parameters here
AS
    SELECT IdPresentacion, DescripcionPresentacion
    FROM TBLPRESENTACION    

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_ubigeo]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_ubigeo]
    @Flag INT,
    @CodigoDepartamento INT,
    @CodigoProvincia INT
-- add more stored procedure parameters here
AS

    SET NOCOUNT ON;

    --Departamentos
    IF @Flag = 1 SELECT IdDepartamento AS Codigo, NombreDepartamento AS Descripcion FROM TBLDEPARTAMENTO
    
    --Provincias de Departamento
    IF @Flag = 2 SELECT IdProvincia AS Codigo, NombreProvincia AS Descripcion FROM TBLPROVINCIA WHERE IdDepartamento = @CodigoDepartamento
    
    --Distritos de Provincia de Departamento
    IF @Flag = 3 SELECT IdDistrito AS Codigo, NombreDistrito AS Descripcion FROM TBLDISTRITO WHERE IdProvincia = @CodigoProvincia AND IdDepartamento = @CodigoDepartamento

GO
/****** Object:  StoredProcedure [dbo].[pa_listar_usuarios]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_usuarios]
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SELECT idUsuario, cuenta, nombre, apellidoPaterno, apellidoMaterno
    FROM TBLPERSONA
    INNER JOIN TBLUSUARIO
    ON TBLPERSONA.idPersona = TBLUSUARIO.IdPersona
GO
/****** Object:  StoredProcedure [dbo].[pa_listar_ventas]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_listar_ventas]
    @FechaInicio VARCHAR(30),
    @FechaFin VARCHAR(30),
    @DatosCliente VARCHAR(500),
    @Estado INT,
    @IdVenta INT
AS
    SET NOCOUNT ON;

    DECLARE
        @sqlQuery NVARCHAR(4000)
    
    SET @sqlQuery = 
    '
        SELECT DISTINCT 
            IdVenta, FechaCreacion, MontoTotal, Subtotal, MontoIGV, 
            u.nombre AS nombreUsuario, u.apellidoPaterno AS aPaternoUsuario, u.apellidoMaterno AS aMaternoUsuario,
            descripcion AS EstadoPago,
            c.numDocumento AS documentoCliente, c.nombre AS nombreCliente, c.apellidoPaterno AS aPaternoCliente, c.apellidoMaterno AS aMaternoCliente
        FROM TBLVENTA
        INNER JOIN TBLUSUARIO
        ON TBLVENTA.IdUsuario = TBLUSUARIO.idUsuario
        INNER JOIN TBLPERSONA u
        ON TBLUSUARIO.IdPersona = u.idPersona
        INNER JOIN TBLCONCEPTO
        ON TBLVENTA.EstadoPago = TBLCONCEPTO.correlativo AND TBLCONCEPTO.prefijo = 6
        INNER JOIN TBLPERSONA c
        ON TBLVENTA.DocumentoCliente = c.numDocumento        
        INNER JOIN v_DatosCliente_ventas 
        ON TBLVENTA.DocumentoCliente = v_DatosCliente_ventas.numDocumento
        WHERE c.tipoRegistro = 2
    '

    IF @FechaInicio != '' AND @FechaFin != ''
        SET @sqlQuery = CONCAT(@sqlQuery, ' AND CONVERT(DATE, FechaCreacion) BETWEEN ''', @FechaInicio,''' AND ''', @FechaFin,'''')
    ELSE
        IF @FechaInicio != ''
            SET @sqlQuery = CONCAT(@sqlQuery, ' AND CONVERT(DATE, FechaCreacion) >= ''', @FechaInicio,'''')
        IF @FechaFin != ''
            SET @sqlQuery = CONCAT(@sqlQuery, ' AND CONVERT(DATE, FechaCreacion) <= ''', @FechaFin,'''')
    
    IF @Estado > 0
        SET @sqlQuery = CONCAT(@sqlQuery, ' AND EstadoPago = ', @Estado)

    IF @IdVenta > 0
        SET @sqlQuery = CONCAT(@sqlQuery, ' AND IdVenta = ', @IdVenta)

    IF @DatosCliente != ''
        SET @sqlQuery = CONCAT(@sqlQuery, ' AND DatosCliente LIKE ''%', @DatosCliente, '%''')

    EXEC (@sqlQuery);





GO
/****** Object:  StoredProcedure [dbo].[pa_registrar_cliente]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_registrar_cliente]
    @Cliente XML,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT
-- add more stored procedure parameters here
AS
    SET NOCOUNT ON;

    DECLARE 
        @CustomerId INT = 0,
        @StatusTran INT = 0

    BEGIN TRAN Tadd

    BEGIN TRY
        
        INSERT TBLPERSONA (
            tipoRegistro, tipoPersona, nombre, apellidoPaterno, apellidoMaterno, tipoDocumento, numDocumento, email, FechaNacimiento, numeroContacto, direccion, idDepartamento, idProvincia, idDistrito
        )
        SELECT 2,
            t.p.value('(TipoPersona)[1]', 'INT'),
            t.p.value('(Nombres)[1]', 'VARCHAR(100)'),
            t.p.value('(ApellidoPaterno)[1]', 'VARCHAR(50)'),
            t.p.value('(ApellidoMaterno)[1]', 'VARCHAR(50)'),
            t.p.value('(TipoDocumento)[1]', 'INT'),
            t.p.value('(NumeroDocumento)[1]', 'VARCHAR(15)'),
            t.p.value('(Correo)[1]', 'VARCHAR(50)'),
            t.p.value('(FechaNacimiento)[1]', 'DATE'),
            t.p.value('(NumeroContacto)[1]', 'VARCHAR(10)'),
            t.p.value('(Direccion)[1]', 'VARCHAR(300)'),
            t.p.value('(CodigoDepartamento)[1]', 'INT'),
            t.p.value('(CodigoProvincia)[1]', 'INT'),
            t.p.value('(CodigoDistrito)[1]', 'INT')
        FROM @Cliente.nodes('AddUpdCustomerRequest') t(p)            

        SET @CustomerId = @@IDENTITY

        IF @@ERROR <> 0 SET @StatusTran = 1        

        IF @StatusTran = 1
            ROLLBACK TRAN Tadd
        ELSE
            SET @Codigo = @CustomerId
            SET @Mensaje = 'Registro OK'
            COMMIT TRAN Tadd

    END TRY

    BEGIN CATCH

        SET @Codigo = -1
        SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
        ROLLBACK TRAN Tadd

    END CATCH



GO
/****** Object:  StoredProcedure [dbo].[pa_registrar_cliente_venta]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_registrar_cliente_venta]
    @Venta XML,
    @Codigo INT = 0 OUT
-- add more stored procedure parameters here
AS
    SET NOCOUNT ON;

    DECLARE
        @CountCustomers INT = 0
    
    BEGIN TRY
    
        SELECT @CountCustomers=COUNT(*) FROM TBLPERSONA WHERE numDocumento IN (
            SELECT
                t.p.value('(NumeroDocumento)[1]', 'VARCHAR(20)')
            FROM @Venta.nodes('AddUpdSaleRequest/Cliente') t(p)
        ) AND tipoPersona = 2

        IF @CountCustomers = 0
            BEGIN 
                INSERT INTO TBLPERSONA (tipoRegistro, tipoDocumento, numDocumento, nombre, apellidoPaterno, apellidoMaterno)
                SELECT 2,
                    t.p.value('(TipoDocumento)[1]', 'INT'),
                    t.p.value('(NumeroDocumento)[1]', 'VARCHAR(20)'),
                    t.p.value('(Nombres)[1]', 'VARCHAR(20)'),
                    t.p.value('(ApellidoPaterno)[1]', 'VARCHAR(20)'),
                    t.p.value('(ApellidoMaterno)[1]', 'VARCHAR(20)')
                FROM @Venta.nodes('AddUpdSaleRequest/Cliente') t(p)

                IF @@ERROR <> 0 SET @Codigo = 1
            END

    END TRY

    BEGIN CATCH

        SET @Codigo = -1

    END CATCH



GO
/****** Object:  StoredProcedure [dbo].[pa_registrar_control_error]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_registrar_control_error]
    @codigo_api int,
    @data_entrada NVARCHAR(300),
    @metodo_error NVARCHAR(50),
    @mensaje_error NVARCHAR(100),
    @traza_error NVARCHAR(500),
    @sucursal INT,
    @documento_usuario NVARCHAR(20),
    @codigo_retorno INT OUT,
    @mensaje_retorno NVARCHAR(100) OUT
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    BEGIN
        SET NOCOUNT ON;

        Begin Tran Tadd

        Begin Try

            INSERT INTO TBLCONTROLERROR 
            (CodigoApi, DataEntrada, MetodoError, MensajeError, TrazaError, Fecha, Sucursal, DocumentoUsuario) 
            VALUES 
            (@codigo_api, @data_entrada, @metodo_error, @mensaje_error, @traza_error,  GETDATE(), @sucursal, @documento_usuario)

            SET @codigo_retorno = 0
            SET @mensaje_retorno = 'Registro correcto.'

            COMMIT TRAN Tadd

        End try
        Begin Catch

            SET @codigo_retorno = 1
            SET @mensaje_retorno = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
            Rollback TRAN Tadd

        End Catch        
    END

GO
/****** Object:  StoredProcedure [dbo].[pa_registrar_pago]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_registrar_pago]
    @Pago XML,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT,
    @Serie VARCHAR(10) OUT,
    @Correlativo INT OUT
-- add more stored procedure parameters here
AS
    SET NOCOUNT ON;

    DECLARE 
        @PaymentId INT = 0,
        @StatusTran INT = 0,
        @TipoComprobante INT = 0,
        @MedioPago INT = 0,
        @CodigoSucursal INT = 0,
        @IdComprobante INT = 0,
        @SaleId INT = 0

    BEGIN TRAN Tadd

    BEGIN TRY
        SET @TipoComprobante = @Pago.value('AddPaymentRequest[1]/TipoComprobante[1]', 'INT');
        SET @MedioPago = @Pago.value('AddPaymentRequest[1]/MedioPago[1]', 'INT');
        SET @CodigoSucursal = @Pago.value('AddPaymentRequest[1]/CodigoSucursal[1]', 'INT');
        SET @SaleId = @Pago.value('AddPaymentRequest[1]/CodigoVenta[1]', 'INT');
        
        BEGIN
            --Factura
            IF @TipoComprobante = 1
                SELECT @IdComprobante = IdComprobante, @Serie = SerieFactura, @Correlativo = CorrelativoFactura + 1 FROM TBLCOMPROBANTE WHERE IdSucursal = @CodigoSucursal
            
            --Boleta
            IF @TipoComprobante = 2
                SELECT @IdComprobante = IdComprobante, @Serie = SerieBoleta, @Correlativo = CorrelativoBoleta + 1 FROM TBLCOMPROBANTE WHERE IdSucursal = @CodigoSucursal
        END

        BEGIN
            --Efectivo
            IF @MedioPago = 1
                INSERT TBLPAGOEFECTIVO (SeriePago, CorrelativoPago, FechaPago, NumeroDocumento, Importe, Efectivo, Vuelto, IdSucursal, IdUsuario, IdVenta)
                SELECT @Serie, @Correlativo,
                    t.p.value('(FechaPago)[1]', 'DATE'),
                    t.p.value('(DocumentoCliente)[1]', 'VARCHAR(15)'),
                    t.p.value('(MontoImporte)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(MontoEfectivo)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(MontoVuelto)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(CodigoSucursal)[1]', 'INT'),
                    t.p.value('(CodigoUsuario)[1]', 'INT'),
                    t.p.value('(CodigoVenta)[1]', 'INT')
                FROM @Pago.nodes('AddPaymentRequest') t(p)

            --Transferencia
            IF @MedioPago = 2
                INSERT TBLPAGOTRANSFERENCIA (SeriePago, CorrelativoPago, FechaPago, NumeroDocumento, Importe, CodigoBanco, NumeroOperacion, IdSucursal, IdUsuario, IdVenta)
                SELECT @Serie, @Correlativo,
                    t.p.value('(FechaPago)[1]', 'DATE'),
                    t.p.value('(DocumentoCliente)[1]', 'VARCHAR(15)'),
                    t.p.value('(MontoImporte)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(CodigoBanco)[1]', 'INT'),
                    t.p.value('(NumeroOperacion)[1]', 'VARCHAR(30)'),
                    t.p.value('(CodigoSucursal)[1]', 'INT'),
                    t.p.value('(CodigoUsuario)[1]', 'INT'),
                    t.p.value('(CodigoVenta)[1]', 'INT')
                FROM @Pago.nodes('AddPaymentRequest') t(p)
        END

        SET @PaymentId = @@IDENTITY

        IF @@ERROR <> 0 
            SET @StatusTran = 1

        ELSE
            BEGIN
                --Actualizar correlativo usado en tabla comprobante
                --Factura
                IF @TipoComprobante = 1
                    UPDATE TBLCOMPROBANTE SET CorrelativoFactura = @Correlativo WHERE IdComprobante = @IdComprobante
                
                --Boleta
                IF @TipoComprobante = 2
                    UPDATE TBLCOMPROBANTE SET CorrelativoBoleta = @Correlativo WHERE IdComprobante = @IdComprobante
            END

            IF @@ERROR <> 0 
                SET @StatusTran = 1
            ELSE
                --Actualizar estado de venta a facturado
                UPDATE TBLVENTA SET EstadoPago = 2 WHERE IdVenta = @SaleId

                IF @@ERROR <> 0 SET @StatusTran = 1

        IF @StatusTran = 1
            ROLLBACK TRAN Tadd
        ELSE
            SET @Codigo = @PaymentId
            SET @Mensaje = 'Registro OK'
            COMMIT TRAN Tadd

    END TRY

    BEGIN CATCH

        SET @Codigo = -1
        SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
        ROLLBACK TRAN Tadd

    END CATCH



GO
/****** Object:  StoredProcedure [dbo].[pa_registrar_producto]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_registrar_producto]
    @Venta XML,
    @Codigo INT = 0 OUT
-- add more stored procedure parameters here
AS
    SET NOCOUNT ON;

    DECLARE
        @ProductId INT = 0,
        @PresentationId INT = 0,
        @SKUProduct NVARCHAR(50),
        @ProductDescription NVARCHAR(100),
        @PresentationDescription NVARCHAR(100),
        @CountRelationPP INT = 0
    
    BEGIN TRY
    
        DECLARE ProductsList CURSOR LOCAL FOR
            SELECT
                t.p.value('(SKU)[1]', 'NVARCHAR(50)'),
                t.p.value('(Descripcion)[1]', 'NVARCHAR(100)'),
                t.p.value('(DescripcionPresentacion)[1]', 'NVARCHAR(100)')
            FROM @Venta.nodes('AddUpdSaleRequest/DetalleVenta/SaleDetail/Producto') t(p)
        
        OPEN ProductsList
        
        FETCH NEXT FROM ProductsList INTO @SKUProduct, @ProductDescription, @PresentationDescription
        
        WHILE (@@FETCH_STATUS = 0)
            BEGIN
                SET @ProductId = 0
                SET @PresentationId = 0

                SELECT @ProductId=IdProducto FROM TBLPRODUCTO WHERE SkuProducto = @SKUProduct
                IF @ProductId = 0
                    BEGIN
                        INSERT INTO TBLPRODUCTO (SkuProducto, DescripcionProducto) VALUES (@SKUProduct, @ProductDescription)
                        SET @ProductId = @@IDENTITY

                        IF @@ERROR <> 0  SET @Codigo = 1
                    END

                --Presentation
                SELECT @PresentationId=IdPresentacion FROM TBLPRESENTACION WHERE DescripcionPresentacion = @PresentationDescription
                IF @PresentationId = 0
                    BEGIN
                        INSERT INTO TBLPRESENTACION (DescripcionPresentacion) VALUES (@PresentationDescription)
                        SET @PresentationId = @@IDENTITY

                        IF @@ERROR <> 0 SET @Codigo = 1
                    END
                
                --Relation Product Presentation
                SELECT @CountRelationPP=COUNT(*) FROM TBLRELACION_P_P WHERE IdProducto = @ProductId AND IdPresentacion = @PresentationId
                IF @CountRelationPP = 0
                    BEGIN
                        INSERT INTO TBLRELACION_P_P (IdProducto, IdPresentacion) VALUES (@ProductId, @PresentationId)
                        
                        IF @@ERROR <> 0 SET @Codigo = 1
                    END
                
                IF @Codigo = 1 BREAK
                FETCH NEXT FROM ProductsList INTO @SKUProduct, @ProductDescription, @PresentationDescription
            END

        CLOSE ProductsList
        DEALLOCATE ProductsList

    END TRY

    BEGIN CATCH

        SET @Codigo = -1

    END CATCH

GO
/****** Object:  StoredProcedure [dbo].[pa_registrar_venta]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pa_registrar_venta] 
    @Venta XML,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE 
            @SaleId INT = 0,
            @StatusTran INT = 0

        BEGIN TRAN Tadd

        BEGIN TRY
            BEGIN
                -- Header
                INSERT TBLVENTA (FechaCreacion, DocumentoCliente, IdSucursal, IdUsuario, TotalArticulos, Subtotal, MontoIGV, MontoTotal)
                SELECT
                    t.p.value('(FechaCreacion)[1]', 'DATETIME'),
                    t.p.value('(Cliente/NumeroDocumento)[1]', 'VARCHAR(20)'),
                    t.p.value('(CodigoSucursal)[1]', 'INT'),
                    t.p.value('(CodigoUsuario)[1]', 'INT'),
                    t.p.value('(NumeroArticulos)[1]', 'INT'),
                    t.p.value('(Subtotal)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(MontoIGV)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(MontoTotal)[1]', 'DECIMAL(10,2)')
                FROM @Venta.nodes('AddUpdSaleRequest') t(p)

                SET @SaleId = @@IDENTITY
                
                IF @@ERROR <> 0
                    SET @StatusTran = 1 
                    
                ELSE                    
                    -- Detail
                    INSERT TBLVENTADETALLE
                    SELECT @SaleId, 
                        t.p.value('(Producto/SKU)[1]', 'NVARCHAR(50)'),
                        t.p.value('(Precio)[1]', 'DECIMAL(10,2)'),
                        t.p.value('(Cantidad)[1]', 'INT'),
                        t.p.value('(Importe)[1]', 'DECIMAL(10,2)'),
                        t.p.value('(Presentacion)[1]', 'NVARCHAR(50)'),
                        t.p.value('(Item)[1]', 'INT')
                    FROM @Venta.nodes('AddUpdSaleRequest/DetalleVenta/SaleDetail') t(p)

                    IF @@ERROR <> 0 
                        SET @StatusTran = 1  

                    ELSE
                        --Customer                        
                        EXEC pa_registrar_cliente_venta @Venta, @StatusTran OUT
            
                        IF @StatusTran <> 1
                            --Product            
                            EXEC pa_registrar_producto @Venta, @StatusTran OUT
            END
            
            IF @StatusTran = 1
                ROLLBACK TRAN Tadd
            ELSE
                SET @Codigo = @SaleId
                SET @Mensaje = 'Registro OK'
                COMMIT TRAN Tadd

        END TRY

        BEGIN CATCH

            SET @Codigo = -1
            SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
            ROLLBACK TRAN Tadd

        END CATCH
    END





GO
/****** Object:  StoredProcedure [dbo].[pa_registrar_venta_version1]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pa_registrar_venta_version1] 
    @Venta XML,
    @Codigo INT = 0 OUT,
    @Mensaje NVARCHAR(500) OUT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE 
            @SaleId INT = 0,
            @ProductId INT = 0,
            @PresentationId INT = 0,
            @StatusTran INT = 0,
            @SKUProduct NVARCHAR(50),
            @ProductDescription NVARCHAR(100),
            @PresentationDescription NVARCHAR(100),
            @CountRelationPP INT = 0,
            @CountCustomers INT = 0

        BEGIN TRAN Tadd

        BEGIN TRY
            -- Header
            INSERT TBLVENTA (FechaCreacion, DocumentoCliente, IdSucursal, IdUsuario, TotalArticulos, Subtotal, MontoTotal)
            SELECT
                t.p.value('(FechaCreacion)[1]', 'DATETIME'),
                t.p.value('(Cliente/NumeroDocumento)[1]', 'VARCHAR(20)'),
                t.p.value('(CodigoSucursal)[1]', 'INT'),
                t.p.value('(CodigoUsuario)[1]', 'INT'),
                t.p.value('(NumeroArticulos)[1]', 'INT'),
                t.p.value('(Subtotal)[1]', 'DECIMAL(10,2)'),
                t.p.value('(MontoTotal)[1]', 'DECIMAL(10,2)')
            FROM @Venta.nodes('AddSaleRequest') t(p)

            SET @SaleId = @@IDENTITY
            
            IF @@ERROR <> 0
                SET @StatusTran = 1 
                
            ELSE
                -- Detail
                INSERT TBLVENTADETALLE
                SELECT @SaleId, 
                    t.p.value('(Producto/SKU)[1]', 'NVARCHAR(50)'),
                    t.p.value('(Precio)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(Cantidad)[1]', 'INT'),
                    t.p.value('(Importe)[1]', 'DECIMAL(10,2)'),
                    t.p.value('(Presentacion)[1]', 'NVARCHAR(50)')
                FROM @Venta.nodes('AddSaleRequest/DetalleVenta/SaleDetail') t(p)

                IF @@ERROR <> 0 
                    SET @StatusTran = 1  

                ELSE
                    -- Customer
                    SELECT @CountCustomers=COUNT(*) FROM TBLPERSONA WHERE numDocumento IN (
                        SELECT
                            t.p.value('(NumeroDocumento)[1]', 'VARCHAR(20)')
                        FROM @Venta.nodes('AddSaleRequest/Cliente') t(p)
                    ) AND tipoPersona = 2

                    IF @CountCustomers = 0
                        BEGIN 
                            INSERT INTO TBLPERSONA (tipoPersona, tipoDocumento, numDocumento, nombre, apellidoPaterno, apellidoMaterno)
                            SELECT 2,
                                t.p.value('(TipoDocumento)[1]', 'INT'),
                                t.p.value('(NumeroDocumento)[1]', 'VARCHAR(20)'),
                                t.p.value('(Nombres)[1]', 'VARCHAR(20)'),
                                t.p.value('(ApellidoPaterno)[1]', 'VARCHAR(20)'),
                                t.p.value('(ApellidoMaterno)[1]', 'VARCHAR(20)')
                            FROM @Venta.nodes('AddSaleRequest/Cliente') t(p)

                            IF @@ERROR <> 0 SET @StatusTran = 1
                        END
                    -- Product
                    DECLARE ProductsList CURSOR LOCAL FOR
                        SELECT
                            t.p.value('(SKU)[1]', 'NVARCHAR(50)'),
                            t.p.value('(Descripcion)[1]', 'NVARCHAR(100)'),
                            t.p.value('(DescripcionPresentacion)[1]', 'NVARCHAR(100)')
                        FROM @Venta.nodes('AddSaleRequest/DetalleVenta/SaleDetail/Producto') t(p)
                    
                    OPEN ProductsList
                    
                    FETCH NEXT FROM ProductsList INTO @SKUProduct, @ProductDescription, @PresentationDescription
                    
                    WHILE (@@FETCH_STATUS = 0)
                        BEGIN
                            SET @ProductId = 0
                            SET @PresentationId = 0

                            SELECT @ProductId=IdProducto FROM TBLPRODUCTO WHERE SkuProducto = @SKUProduct
                            IF @ProductId = 0
                                BEGIN
                                    INSERT INTO TBLPRODUCTO (SkuProducto, DescripcionProducto) VALUES (@SKUProduct, @ProductDescription)
                                    SET @ProductId = @@IDENTITY

                                    IF @@ERROR <> 0  SET @StatusTran = 1
                                END

                            --Presentation
                            SELECT @PresentationId=IdPresentacion FROM TBLPRESENTACION WHERE DescripcionPresentacion = @PresentationDescription
                            IF @PresentationId = 0
                                BEGIN
                                    INSERT INTO TBLPRESENTACION (DescripcionPresentacion) VALUES (@PresentationDescription)
                                    SET @PresentationId = @@IDENTITY

                                    IF @@ERROR <> 0 SET @StatusTran = 1
                                END
                            
                            --Relation Product Presentation
                            SELECT @CountRelationPP=COUNT(*) FROM TBLRELACION_P_P WHERE IdProducto = @ProductId AND IdPresentacion = @PresentationId
                            IF @CountRelationPP = 0
                                BEGIN
                                    INSERT INTO TBLRELACION_P_P (IdProducto, IdPresentacion) VALUES (@ProductId, @PresentationId)
                                    
                                    IF @@ERROR <> 0 SET @StatusTran = 1
                                END
                            
                            IF @StatusTran = 1 BREAK
                            FETCH NEXT FROM ProductsList INTO @SKUProduct, @ProductDescription, @PresentationDescription
                        END

                    CLOSE ProductsList
                    DEALLOCATE ProductsList
            
            IF @StatusTran = 1
                ROLLBACK TRAN Tadd
            ELSE
                SET @Codigo = @SaleId
                SET @Mensaje = 'Registro OK'
                COMMIT TRAN Tadd

        END TRY

        BEGIN CATCH

            SET @Codigo = -1
            SET @Mensaje = 'Ocurrio un Error: ' + ERROR_MESSAGE() + ' en la línea ' + CONVERT(NVARCHAR(255), ERROR_LINE() ) + '.'
            ROLLBACK TRAN Tadd

        END CATCH
    END


GO
/****** Object:  StoredProcedure [dbo].[pa_validar_cuenta_usuario]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[pa_validar_cuenta_usuario]
    @cuenta /*parameter name*/ varchar(50)
-- add more stored procedure parameters here
AS
    -- body of the stored procedure
    SELECT idUsuario, idPerfil, documento, clave, sal
    FROM TBLUSUARIO
    WHERE cuenta = @cuenta

GO
/****** Object:  StoredProcedure [dbo].[SP_ACCESOMODULO]    Script Date: 07/04/2021 06:20:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ACCESOMODULO]
    @idperfil int,   
    @idusuario int
AS  
BEGIN
 DECLARE @usuario int,
  @modulo int,
  @submodulo int

 SET NOCOUNT ON;
 --OBTENER EL USUARIO
 select @usuario=idUsuario from tblrelacion_p_m where idperfil= @idperfil order by idusuario desc

 if @usuario=9999
 BEGIN 
	 select @modulo=idModulo from tblrelacion_p_m where idperfil= @idperfil and idusuario=@usuario
	 if @modulo=9999
	 BEGIN 
		select idPerfilModulo,idPerfil,idUsuario, 
idModulo,(select nombre from tblmodulo where idModulo=tblrelacion_p_m.idModulo ) as nombre,
idSubmodulo,(select nombre from tblsubmodulo where idSubModulo=tblrelacion_p_m.idSubmodulo ) as nombreSub
from tblrelacion_p_m 
		where idperfil= @idperfil and idusuario=@usuario and idmodulo=@modulo
		END
	 else
		BEGIN
		select idPerfilModulo,idPerfil,idUsuario, 
idModulo,(select nombre from tblmodulo where idModulo=tblrelacion_p_m.idModulo ) as nombre,
idSubmodulo,(select nombre from tblsubmodulo where idSubModulo=tblrelacion_p_m.idSubmodulo ) as nombreSub
from tblrelacion_p_m 
where idperfil= @idperfil and idusuario=@usuario and idmodulo=@modulo and idSubmodulo<>9999
		END
END
 else
 BEGIN 
	 select @modulo=idModulo from tblrelacion_p_m where idperfil= @idperfil and idusuario=@usuario
	 if @modulo=9999
	 BEGIN 
		select idPerfilModulo,idPerfil,idUsuario, 
idModulo,(select nombre from tblmodulo where idModulo=tblrelacion_p_m.idModulo ) as nombre,
idSubmodulo,(select nombre from tblsubmodulo where idSubModulo=tblrelacion_p_m.idSubmodulo ) as nombreSub
from tblrelacion_p_m 
where idperfil= @idperfil and idusuario=@usuario and idmodulo=@modulo
		END
	 else
		BEGIN
		select idPerfilModulo,idPerfil,idUsuario, 
idModulo,(select nombre from tblmodulo where idModulo=tblrelacion_p_m.idModulo ) as nombre,
idSubmodulo,(select nombre from tblsubmodulo where idSubModulo=tblrelacion_p_m.idSubmodulo ) as nombreSub
from tblrelacion_p_m 
where idperfil= @idperfil and idusuario=@usuario and idmodulo=@modulo and idSubmodulo<>9999
		END
END
END

GO
USE [master]
GO
ALTER DATABASE [db_sistema_erp] SET  READ_WRITE 
GO
